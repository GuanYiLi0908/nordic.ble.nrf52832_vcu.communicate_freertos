

#include "Protocol_PackageDefine.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define FakeInitTable	1
#define Array2				{0x55, 0x55}
#define Array3				{0x55, 0x55, 0x55}
#define Array4				{0x55, 0x55, 0x55, 0x55}
#define Array5				{0x55, 0x55, 0x55, 0x55, 0x55}
#define Array7				{0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55}
#define Array8				{0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55}

VCUCONTROLREQ	sVCUCtrlReq_01;	//Not Initialzation

CELLPHONECONTROLREQ	sPhoneCtrlReq_31; //Not Initialzation
CELLPHONECONTROLRSP	sPhoneCtrlRsp_B1; //Not Initialzation

BLUETOOTHCONTROLRSP	sBLECtrlRsp_A9;	//Not Initialzation

VCUHARDWARESTATUS	 sVCUHwStatus_89 = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tVCUHwStatus,
	.Length			=	0x1B,		

#ifdef FakeInitTable	
	/* Group 1 - Mode Status */
	.DynamicModeComman 			= 5,
	.RegeonModeComman				= 3,
	.ThrottleOpenedPosition = Array2,
	.Group1Reserved 				= Array4,

	/* Group 2 - Immediate Informaiotn */
	.DC12v 									= Array2,
	.B12v 									= Array2,
	.Speed 									= Array2,
	.ChargeState 						= 0x55,
	.Group2Reserved 				= 0x55,
			
	/* Group 3 - Switch Status */
	.DiagnosticState			 	= 1,
	.CollisionProtection	 	= 0,
	.RolloverProtection			= 1,
			
	.KeySwitch							= 0,
	.TouchEN								= 1,
	.HandleBrakeSW_R				= 0,
	.HandleBrakeSW_L				= 1,
	.FootBrakeSW						= 0,
	.TrunkBState						= 1,
	.ParkingState						= 0,
	.WiperSwitch						= 1,
			
	.HeadLight							= 0,
	.DRL_Front							= 1,
	.DRL_Rear								= 0,
	.Winker_Rear						= 1,
	.Winker_Front						= 0,
			
	.BrakeLight_Rear				= 1,
	.HazardLight						= 0,
				
	.Group3Reserved					= Array4
#endif			
};

VCUMILEAGEINFORMATION	sVCUMileageErr_8A = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tVCUMileageAndErr,
	.Length			=	0x1B,		
	
#ifdef FakeInitTable	
	/* Group 1 - Mileage Information */		
	.TotalMileage						= Array3,
	.TripMileage						= Array3,
	.RangeMileage						= Array2,
			
	/* Group 2 - VCU Faults and Error */
	.Group2Reserved					= Array8,
			
	/* Group 3 - Reserved */ 
	.Group3Reserved					= Array8,				
#endif
};

VCUBASICINFO	sVCUBasicInfo_8B = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tVCUBasciInfo,
	.Length			=	0x1B,		
	
#ifdef FakeInitTable
	/* Group 1 - VCU Manufacture information */ 
	.ManufacYear						= 0x55,
	.ManufacMonth						= 0x55,
	.ManufacID							= 0x55,
	.VCU_PN									= 0x55,
	.VCU_SN									= Array4,
			
	/* Group 2 - VCU Sw/Hw Version information */
	.HwVersion							= Array2,
	.SwVersion							= Array2,
	.BootVersion						= Array2,
	.Group2Reserved					= Array2,
			
	/* Group 3 - Reserved */ 
	.Group3Reserved					= Array8
#endif
};

DRIVERHARDWARESTATUS	sDrvHwStatus_91 = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tDrvHwStatus,
	.Length			=	0x1B,		
	
#ifdef FakeInitTable
	/* Group 1 - Mode Status */
	.DynamicModeStatus			= 5,
			
	.DCValtage							= Array2,
	.DCCurrent							= Array2,
	.PhaseCurrent						= Array2,
			
	/* Group 2 - Working Status */
	.MotorTemp							= 0x55,
	.DriverTemp							= 0x55,
	.MotorRPM								= Array2,
	.MotorAngle							= Array2,
			
	/* Group 3 - Error Status */				
	.Group3Reserved					= Array8,
#endif	
};

DRIVERBASICINFO		sDrvBasicInfo_92 = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tDrvBasciInfo,
	.Length			=	0x1B,		
	
#ifdef FakeInitTable
	/* Group 1 - Driver Manufacture information */ 
	.ManufacYear						= 0x55,
	.ManufacMonth						= 0x55,
	.ManufacID							= 0x55,
	.Drv_PN									= 0x55,
	.Drv_SN									= Array4,
			
	/* Group 2 - Driver Sw/Hw Version information */
	.HwVersion							= Array2,
	.SwVersion							= Array2,
	.BootVersion						= Array2,
	.Group2Reserved					= Array2,
			
	/* Group 3 - Reserved */ 
	.Group3Reserved					= Array8,
#endif	
};

BMSSYSTEMINFORMATION	sBMSSysInfo_99 = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tBMSSysStatus,
	.Length			=	0x1B,
	
#ifdef FakeInitTable
	/* Group 1 - Battery System Status */
	.SystemCapacity					= Array2,
	.SystemVoltage					= Array2,
	.SystemCurrent					= Array2,
	.SysAvailableCap				= Array2,
			
	/* Group 2 - Battery System Limitation */
	.SysDischargeCurntLimit	= Array2,
	.SysChargeCurntLimit		= Array2,
	.Group2Reserved					= Array4,
			
	/* Group 3 - Reserved */ 
	.Group3Reserved					= Array8,
#endif	
};

BMSGROUPINFORMATION sBMSGroupInfo_9A = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tBMSGroupStatus,
	.Length			=	0x1B,
	
#ifdef FakeInitTable
	/* Group 1 - Battery Group 1 Status */
	.Group1_Capacity				= Array2,
	.Group1_Voltage					= Array2,
	.Group1_Current					= Array2,
	.Group1_AvailableCap		= Array2,
		
	/* Group 2 - Battery Group 2 Status */
	.Group2_Capacity				= Array2,
	.Group2_Voltage					= Array2,
	.Group2_Current					= Array2,
	.Group2_AvailableCap		= Array2,
			
	/* Group 3 - Battery Group Limitation */
	.Group1_DischargeCurntLimit	= Array2,
	.Group1_ChargeCurntLimit		= Array2,
	.Group2_DischargeCurntLimit	= Array2,
	.Group2_ChargeCurntLimit		= Array2,
#endif	
};	

BMSPACKAGEINFORMATION1 sBMSPackageInfo1_9B = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tBMSPackageStatus_1,
	.Length			=	0x1B,
	
#ifdef FakeInitTable
	/* Group 1 - Battery Package 1 Status */
	.Pack1_SOC									= 0x55,
	.Pack1_Capacity							= Array2,
	.Pack1_Voltage							= Array2,
	.Pack1_Current							= Array2,
	.Group1Reserved							=	0x55,
		
	/* Group 2 - Battery Package 1 Limitation */
	.Pack1_MaxCellTemp					=	0x55,
	.Pack1_MinCellTemp					= 0x55,
	.Pack1_MaxMOSTemp						= 0x55,
		
	.Pack1_ChargeMOS						= 1,
	.Pack1_DischargeMOS					= 0,
			
	.Pack1_MaxCellVol						= Array2,
	.Pack1_MinCellVol						= Array2,
			
	/* Group 3 - Battery Package 2 Status */
	.Pack2_SOC									= 0x55,
	.Pack2_Capacity							= Array2,
	.Pack2_Voltage							= Array2,
	.Pack2_Current							= Array2,
	.Group3Reserved							= 0x55,
#endif	
};

BMSPACKAGEINFORMATION2 sBMSPackageInfo2_9C = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tBMSPackageStatus_2,
	.Length			=	0x1B,
	
#ifdef FakeInitTable
	/* Group 1 - Battery Package 2 Limitation */
	.Pack2_MaxCellTemp					= 0x55,
	.Pack2_MinCellTemp					= 0x55,
	.Pack2_MaxMOSTemp						= 0x55,
			
	.Pack2_ChargeMOS						= 1,
	.Pack2_DischargeMOS					= 0,
			
	.Pack2_MaxCellVol						= Array2,
	.Pack2_MinCellVol						= Array2,
		
	/* Group 2 - Battery Package 3 Status */
	.Pack3_SOC									= 0x55,
	.Pack3_Capacity							= Array2,
	.Pack3_Voltage							= Array2,
	.Pack3_Current							= Array2,
	.Group2Reserved							= 0x55,
			
	/* Group 3 - Battery Package 2 Status */
	.Pack3_MaxCellTemp					= 0x55,
	.Pack3_MinCellTemp					= 0x55,
	.Pack3_MaxMOSTemp						= 0x55,
			
	.Pack3_ChargeMOS						= 0,
	.Pack3_DischargeMOS					= 1,
			
	.Pack3_MaxCellVol						= Array2,
	.Pack3_MinCellVol						= Array2,
#endif	
};

BMSPACKAGEINFORMATION3 sBMSPackageInfo3_9D = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tBMSPackageStatus_3,
	.Length			=	0x1B,
	
#ifdef FakeInitTable
	/* Group 1 - Battery Package 4 Status */
	.Pack4_SOC									= 0x55,
	.Pack4_Capacity							= Array2,
	.Pack4_Voltage							= Array2,
	.Pack4_Current							= Array2,
	.Group1Reserved							= 0x55,
			
	/* Group 2 - Battery Package 3 Limitation */
	.Pack4_MaxCellTemp					= 0x55,
	.Pack4_MinCellTemp					= 0x55,
	.Pack4_MaxMOSTemp						= 0x55,
		
	.Pack4_ChargeMOS						= 1,
	.Pack4_DischargeMOS					= 0,
			
	.Pack4_MaxCellVol						= Array2,
	.Pack4_MinCellVol						= Array2,
			
	/* Group 3 - Battery Failure Information */
	.Group3Reserved							= Array8,
#endif	
};

TPMSINFORMATION sTPMSinfo_B9 = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tTPMSInfo,
	.Length			=	0x1B,
	
#ifdef FakeInitTable
	/* Group 1 - TPMS Sensor 1 Status */
	.Sensor1_DeviceID						= Array4,
	.Sensor1_Pressure						= 0x55,
	.Serson1_Temper							= 0x55,	
	.Serson1_StatusReserved			= Array2,
			
	/* Group 2 - TPMS Sensor 2 Status */
	.Sensor2_DeviceID						= Array4,
	.Sensor2_Pressure						= 0x55,
	.Serson2_Temper							= 0x55,	
	.Serson2_StatusReserved			= Array2,
		
	/* Group 3 - TPMS Sensor 3 Status */
	.Sensor3_DeviceID						= Array4,
	.Sensor3_Pressure						= 0x55,
	.Serson3_Temper							= 0x55,	
	.Serson3_StatusReserved			= Array2,		
#endif	
};

TPMSMANUFACINFORMATION sTPMSManuFacInfo_BA = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tTPMSManufacInfo,
	.Length			=	0x1B,
	
#ifdef FakeInitTable
	/* Group 1 - TManufacturing Information */
	.ManufacYear								= 0x55,	
	.ManufacMonth								= 0x55,	
	.ManufacID									= 0x55,	
	.TPMS_PN										= 0x55,	
	.TPMS_SN										= Array4,	
			
	/* Group 2 - Reserved */ 
	.HwVersion									= Array2,		
	.SwVersion									= Array2,		
	.BootVersion								= Array2,		
	.Group2Reserved							= Array2,		
			
	/* Group 3 - Reserved */ 
	.Group3Reserved						= Array8,		
#endif	
};

EMERGENCYERRORINFO sEmergencyErr_D9 = 
{
	.OBD_Header = ohBluetooth,
	.CategoryID = tEmergencyErrInfo,
	.Length			=	0x1B,

#ifdef FakeInitTable
	
	/* Group 1 */ 
	//#0
	.Battery12VoltageErr				= 1,
	.LCD12vErr									= 0,
	.BatteryPack12vErr					= 1,
	.BatterySecurityErr					= 0,
	.Battery12vChargerErr				= 1,
	.Horn12vErr									= 0,
	.Fuse12vErr									= 1,
	.MCU12vErr									= 0,
			
	//#1
	.DC1VoltageErr							= 1,
	.Throttle5vErr							= 0,
	.TP12vErr										= 1,
	.BatterySysSOClow						= 0,
	.DC2VoltageErr							=	1,
	.TPSVoltageLow							= 0,
	.TPSVoltageHigh							= 1,
	.TPSErr											= 0,			

	//#2
	.MCUCAN0Offline							= 1,
	.DashboardCAN1Offline				= 0,
	.BatterySysCAN0Offline			= 1,
	.BatterySysCurrentErr				= 0,
	.TurnkOpen									= 1,
	.TPMSOffline								= 0,
	.Head12vErr									= 1,

	//#3
	.PackErr_P3001							= 0,
	.PackErr_P3002							= 1,
	.PackErr_P3003							= 0,
	.PackErr_P3004							= 1,
	.PackErr_P3005							= 0,
	.PackErr_P3006							= 1,
	.PackErr_P3007							= 0,
	.PackErr_P3008							= 1,
			
	//#4
	.PackErr_P3009							= 0,
	.PackErr_P3010							= 1,
	.PackErr_P3011							= 0,
	.PackErr_P3012							= 1,
	.PackErr_P3013							= 0,
	.PackErr_P3014							= 1,
	.PackErr_P3015							= 0,
			
	//#5
	.SysCANCommuniFault					= 1,
	.BMSNFCCommuniFault					= 0,
			
	//#6
	.ABIEncoderErr							= 1,
	.PWMEncoderErr							= 0,
	.DriverOVP									= 1,
	.DriverUVP									= 0,
	.iPHHwOCP										= 1,
	.iDCHwOCP										= 0,
	.MCUTPSErr									= 1,
	.PreChargeErr								= 0,
		
	//#7
	.iPHSwOCP										= 1,
	.iDCSwOCP										= 0,
	.MotorOTP										= 1,
	.DriverOTP									= 0,
	.BlockProtect								= 1,
	.VCUCommuniFault						= 0,
	.EEPROMErr									= 1,

	/* Group 2 */ 
	.MotorHighTempAlarm					= 0,
	.DriverHighTempAlarm				= 1,
	.TPMSTempErr								= 0,
	.TPMSBatteryLow							= 1,
	.Group2Reserved							= Array7,
			
	/* Group 3 - Reserved */ 
	.Group3Reserved							= Array8,	
#endif	
};
