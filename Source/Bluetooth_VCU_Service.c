

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#include "sdk_common.h"
#include "ble.h"
#include "ble_srv_common.h"

#include "Bluetooth_VCU_Service.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

uint8_t VCU_BASE_UUID[16] =	{ 0x68, 0xE1, 0x93, 0x15, 0x33, 0x51, 0x29, 0x87, 0x6A, 0x4D, 0x4A, 0x7C, 0x45, 0x0C, 0xE6, 0xB8 };

uint32_t Bluetooth_VCUs_Initialization(ble_vcus_t * p_VCUs, const ble_vcus_init_t * p_VCUs_init)
{	
    uint32_t   						err_code;
    ble_uuid_t 						ble_uuid;
		ble_uuid128_t 				base_uuid;
		ble_add_char_params_t add_char_params;
		
		ble_gap_addr_t 						GAP_Addr;
		/* Get Device MAC Address */
		err_code = sd_ble_gap_addr_get(&GAP_Addr);
		if(err_code != NRF_SUCCESS)
			NRF_LOG_ERROR("sd_ble_gap_addr_get Fall !!!");
		
		memcpy(VCU_BASE_UUID, GAP_Addr.addr, 6);
		NRF_LOG_DEBUG("Device MAC Address : %02X %02X %02X %02X %02X %02X",	GAP_Addr.addr[0],
																																				GAP_Addr.addr[1],
																																				GAP_Addr.addr[2],
																																				GAP_Addr.addr[3],
																																				GAP_Addr.addr[4],
																																				GAP_Addr.addr[5]);
		memcpy(&base_uuid, VCU_BASE_UUID, 16);
	
		VERIFY_PARAM_NOT_NULL(p_VCUs);
    VERIFY_PARAM_NOT_NULL(p_VCUs_init);
		
		/* Initialize service structure */
		p_VCUs -> evt_handler               = p_VCUs_init -> evt_handler;

    /* Add Custom Service UUID */
    err_code =  sd_ble_uuid_vs_add(&base_uuid, &p_VCUs -> uuid_type);
    VERIFY_SUCCESS(err_code);
    
    ble_uuid.type = p_VCUs -> uuid_type;
    ble_uuid.uuid = VCU_SERVICE_UUID;

    /* Add the Custom Service */
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_VCUs -> service_handle);
    VERIFY_SUCCESS(err_code);
		
		/* Add the Custom Service RX Characteristic. */
		memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              				= BLE_UUID_VCUS_RX_CHARACTERISTIC;
    add_char_params.uuid_type         				= p_VCUs -> uuid_type;
    add_char_params.max_len           				= BLE_VCUS_MAX_DATA_LEN;
    add_char_params.init_len          				= sizeof(uint8_t);
    add_char_params.is_var_len        				= true;
		add_char_params.is_value_user							= BLE_GATTS_VLOC_STACK;
		add_char_params.char_props.write					= 1;
		add_char_params.char_props.write_wo_resp	= 1;

    add_char_params.read_access       = SEC_OPEN;
    add_char_params.write_access      = SEC_OPEN;
		
		err_code = characteristic_add(p_VCUs -> service_handle, &add_char_params, &p_VCUs -> VCUs_Rx_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

		/* Add Custom Service TX characteristic */
    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = BLE_UUID_VCUS_TX_CHARACTERISTIC;
    add_char_params.uuid_type         = p_VCUs -> uuid_type;
    add_char_params.max_len           = BLE_VCUS_MAX_DATA_LEN;
    add_char_params.init_len          = sizeof(uint8_t);
    add_char_params.is_var_len        = true;		
    add_char_params.char_props.notify = 1;

    add_char_params.read_access       = SEC_OPEN;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.cccd_write_access = SEC_OPEN;
		
    return characteristic_add(p_VCUs -> service_handle, &add_char_params, &p_VCUs -> VCUs_Tx_handles);
}

void ble_vcus_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    if ((p_context == NULL) || (p_ble_evt == NULL))
    {
        return;
    }

    ble_vcus_t * p_vcus = (ble_vcus_t *)p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_vcus, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_vcus, p_ble_evt);
            break;

        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
            on_hvx_tx_complete(p_vcus, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_vcus      Vehicle Control Unit Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_vcus_t * p_vcus, ble_evt_t const * p_ble_evt)
{
    ret_code_t                 	err_code;
    ble_vcus_evt_t             	evt;
    ble_gatts_value_t          	gatts_val;
    uint8_t                    	cccd_value[2];
    ble_vcus_client_context_t * p_client = NULL;

    err_code = blcm_link_ctx_get(p_vcus -> p_link_ctx_storage,
                                 p_ble_evt->evt.gap_evt.conn_handle,
                                 (void *) &p_client);
    if (err_code != NRF_SUCCESS)
    {
			NRF_LOG_ERROR("on_connect : %02X", err_code);
      NRF_LOG_ERROR("Link context for 0x%02X connection handle could not be fetched.",
                      p_ble_evt->evt.gap_evt.conn_handle);
    }

    /* Check the hosts CCCD value to inform of readiness to send data using the RX characteristic */
    memset(&gatts_val, 0, sizeof(ble_gatts_value_t));
    gatts_val.p_value = cccd_value;
    gatts_val.len     = sizeof(cccd_value);
    gatts_val.offset  = 0;

    err_code = sd_ble_gatts_value_get(p_ble_evt -> evt.gap_evt.conn_handle,
                                      p_vcus -> VCUs_Rx_handles.cccd_handle,
                                      &gatts_val);

    if ((err_code == NRF_SUCCESS)     &&
        (p_vcus -> evt_handler != NULL) &&
        ble_srv_is_notification_enabled(gatts_val.p_value))
    {
        if (p_client != NULL)
        {
            p_client->is_notification_enabled = true;
        }

        memset(&evt, 0, sizeof(ble_vcus_evt_t));
        evt.type       	= BLE_VCUS_EVT_COMM_STARTED;
        evt.p_vcus      = p_vcus;
        evt.conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
        evt.p_link_ctx  = p_client;

        p_vcus -> evt_handler(&evt);
    }
}


/**@brief Function for handling the Disconnect event.
 *
 * @param[in]   p_vcus      Vehicle Control Unit Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_vcus_t * p_vcus, ble_evt_t const * p_ble_evt)
{
//    UNUSED_PARAMETER(p_ble_evt);
//    p_vcus->conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief Function for handling the Write event.
 *
 * @param[in]   p_cus       Custom Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_vcus_t * p_vcus, ble_evt_t const * p_ble_evt)
{
    ret_code_t                     err_code;
    ble_vcus_evt_t                 evt;
    ble_vcus_client_context_t    * p_client;
    ble_gatts_evt_write_t const *  p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
	
    err_code = blcm_link_ctx_get(p_vcus->p_link_ctx_storage,
                                 p_ble_evt->evt.gatts_evt.conn_handle,
                                 (void *) &p_client);
    if (err_code != NRF_SUCCESS)
    {
			NRF_LOG_ERROR("on_write : %02X", err_code);
      NRF_LOG_ERROR("Link context for 0x%02X connection handle could not be fetched.",
                      p_ble_evt->evt.gatts_evt.conn_handle);
    }

    memset(&evt, 0, sizeof(ble_vcus_evt_t));
    evt.p_vcus       = p_vcus;
    evt.conn_handle  = p_ble_evt->evt.gatts_evt.conn_handle;
    evt.p_link_ctx   = p_client;

    if ((p_evt_write->handle == p_vcus -> VCUs_Tx_handles.cccd_handle) &&
        (p_evt_write->len == 2))
    {
        if (p_client != NULL)
        {
            if (ble_srv_is_notification_enabled(p_evt_write->data))
            {
                p_client->is_notification_enabled = true;
                evt.type                          = BLE_VCUS_EVT_COMM_STARTED;
            }
            else
            {
                p_client->is_notification_enabled = false;
                evt.type                          = BLE_VCUS_EVT_COMM_STOPPED;
            }

            if (p_vcus -> evt_handler != NULL)
            {
                p_vcus -> evt_handler(&evt);
            }

        }
    }
    else if ((p_evt_write->handle == p_vcus -> VCUs_Rx_handles.value_handle) &&
             (p_vcus -> evt_handler != NULL))
    {
        evt.type                  = BLE_VCUS_EVT_RX_DATA;
        evt.params.rx_data.p_data = p_evt_write->data;
        evt.params.rx_data.length = p_evt_write->len;

        p_vcus -> evt_handler(&evt);
    }
    else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}

/**@brief Function for handling the @ref BLE_GATTS_EVT_HVN_TX_COMPLETE event from the SoftDevice.
 *
 * @param[in] p_nus     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_hvx_tx_complete(ble_vcus_t * p_vcus, ble_evt_t const * p_ble_evt)
{
    ret_code_t                  err_code;
    ble_vcus_evt_t              evt;
    ble_vcus_client_context_t * p_client;

    err_code = blcm_link_ctx_get(p_vcus->p_link_ctx_storage,
                                 p_ble_evt->evt.gatts_evt.conn_handle,
                                 (void *) &p_client);
    if (err_code != NRF_SUCCESS)
    {
			NRF_LOG_ERROR("on_hvx_tx_complete : %02X", err_code);
      NRF_LOG_ERROR("Link context for 0x%02X connection handle could not be fetched.",
                      p_ble_evt->evt.gatts_evt.conn_handle);
      return;
    }

    if (p_client->is_notification_enabled)
    {
        memset(&evt, 0, sizeof(ble_vcus_evt_t));
        evt.type        = BLE_VCUS_EVT_TX_RDY;
        evt.p_vcus       = p_vcus;
        evt.conn_handle = p_ble_evt->evt.gatts_evt.conn_handle;
        evt.p_link_ctx  = p_client;

        p_vcus -> evt_handler(&evt);
    }
}

uint32_t ble_vcus_data_send(ble_vcus_t * p_vcus,
                            uint8_t   	* p_data,
                            uint16_t  	* p_length,
                            uint16_t    conn_handle)
{
    ret_code_t                 err_code;
    ble_gatts_hvx_params_t     hvx_params;
    ble_vcus_client_context_t * p_client;

    VERIFY_PARAM_NOT_NULL(p_vcus);

    err_code = blcm_link_ctx_get(p_vcus->p_link_ctx_storage, conn_handle, (void *) &p_client);
    VERIFY_SUCCESS(err_code);

    if ((conn_handle == BLE_CONN_HANDLE_INVALID) || (p_client == NULL))
    {
        return NRF_ERROR_NOT_FOUND;
    }

    if (!p_client->is_notification_enabled)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    if (*p_length > BLE_VCUS_MAX_DATA_LEN)
    {
        return NRF_ERROR_INVALID_PARAM;
    }

    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_vcus -> VCUs_Tx_handles.value_handle;
    hvx_params.p_data = p_data;
    hvx_params.p_len  = p_length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

    return sd_ble_gatts_hvx(conn_handle, &hvx_params);
}
