

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "bsp_btn_ble.h"
#include "crc32.h"

#include "Communication_UART.h"
#include "TPMS.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

extern QueueHandle_t Q_TPMStoUART;
extern QueueHandle_t Q_UARTDataRx;
extern SemaphoreHandle_t Mtx_UARTTransmit;

extern VCUCONTROLREQ					sVCUCtrlReq_01;
extern VCUHARDWARESTATUS	 		sVCUHwStatus_89;
extern VCUMILEAGEINFORMATION	sVCUMileageErr_8A;
extern VCUBASICINFO						sVCUBasicInfo_8B;
extern DRIVERHARDWARESTATUS		sDrvHwStatus_91;
extern DRIVERBASICINFO				sDrvBasicInfo_92;
extern BMSSYSTEMINFORMATION		sBMSSysInfo_99;
extern BMSGROUPINFORMATION 		sBMSGroupInfo_9A;
extern BMSPACKAGEINFORMATION1 sBMSPackageInfo1_9B;
extern BMSPACKAGEINFORMATION2 sBMSPackageInfo2_9C;
extern BMSPACKAGEINFORMATION3 sBMSPackageInfo3_9D;
extern TPMSINFORMATION 				sTPMSinfo_B9;
extern TPMSMANUFACINFORMATION sTPMSManuFacInfo_BA;
extern EMERGENCYERRORINFO 		sEmergencyErr_D9;

TaskHandle_t N_VCUCtrlReq = NULL;


static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;  		/**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static const uint32_t PreviousCRC_Buf = NULL; 

static uint8_t UARTDataRx_Array[PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH] = { 0 };

/******************************************************************************/
/*                 	  	 						UART Task						                	  	*/
/******************************************************************************/
void UARTTask(void *pvParameters)
{
	uint8_t res = 0;
	uint8_t tempQueueData_Array[PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH] = { 0 };
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
//	uint8_t tempUARTTxData_Array[UART_TRANSMITT_BASIC_DATA_LENGTH + DATA_CRC32_LENGTH] = { 0 };

	while(1)
	{
		
		vTaskDelay(1);
	}
}


static uint8_t UART_PackageHeaderDetermine(UARTTASK *p_Task)
{
	BaseType_t err;
	uint8_t Res = PROCESS_UNWORKING;
	
	switch(p_Task -> eHeaderProcess)
	{
		case Pkg_Header:
			if(Q_UARTDataRx != NULL)
			{
				err = xQueueReceive(Q_UARTDataRx, &p_Task -> sPackage.OBDHeader, 0);
				if(err == pdPASS)
				{
//					NRF_LOG_DEBUG("sPackageHeader.OBDHeader : %X", p_Task -> sPackage.OBDHeader);
					if(p_Task -> sPackage.OBDHeader == ohVCU)
					{
						p_Task -> eHeaderProcess = Pkg_CategoryID;
					}
					else
						p_Task -> eHeaderProcess = Pkg_Header;					
				}
			}
			break;
		
		case Pkg_CategoryID:
			err = xQueueReceive(Q_UARTDataRx, &p_Task -> sPackage.CategoryID, 0);
			if(err == pdPASS)
			{
//				NRF_LOG_DEBUG("sPackageHeader.CategoryID : %X", p_Task -> sPackage.CategoryID);
				if((p_Task -> sPackage.CategoryID == cVCUCtrl_Req) ||
					 (p_Task -> sPackage.CategoryID == cVCUCtrl_Rsp))
				{
					p_Task -> eHeaderProcess = Pkg_Length;
				}
				else
				{	
					Res = PROCESS_ERR_UNKNOW_COMMAND;
					p_Task -> eHeaderProcess = Pkg_Header;
				}				
			}
			else
			{
				NRF_LOG_DEBUG("UART_PackageCategoryID Receive Error");
				p_Task -> eHeaderProcess = Pkg_Header;
			}
			break;
		
		case Pkg_Length:
			break;
		
		case Pkg_Data:
			break;
	}
}
/******************************************************************************/
/*                 	  			UART Function subroutine		                	  	*/
/******************************************************************************/
/**@brief Function for initializing the UART. */
void UART_Initialization(void)
{
    ret_code_t err_code;

    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       UARTEvent_handler,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);

    APP_ERROR_CHECK(err_code);
}

/**@brief   Function for handling app_uart events.
 *
 * @details This function receives a single character from the app_uart module and appends it to
 *          a string. The string is sent over BLE when the last character received is a
 *          'new line' '\n' (hex 0x0A) or if the string reaches the maximum data length.
 */
static void UARTEvent_handler(app_uart_evt_t * p_event)
{
//    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
    uint32_t       err_code;
		BaseType_t xHigherPriorityTaskWoken;
		BaseType_t err;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
					UNUSED_VARIABLE(app_uart_get(&UARTDataRx_Array[index]));
					index++;

					if(index >= PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH)
					{
						
#ifdef forUARTRxNRF_LOG							
						NRF_LOG_DEBUG("UARTDataRx_Array :");
						for(uint8_t i = 0; i < PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH; i++)
						{
							NRF_LOG_RAW_INFO(" %02X ", UARTDataRx_Array[i]);
						}
						NRF_LOG_RAW_INFO("\r\n");
#endif					
						
						if(Q_UARTDataRx != NULL)
						{
							err = xQueueSendFromISR(Q_UARTDataRx, &UARTDataRx_Array, &xHigherPriorityTaskWoken);
							if(err == errQUEUE_FULL)
							{
								NRF_LOG_ERROR("Q_UARTDataRx if Full");
							}
							memset(UARTDataRx_Array, 0, PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH);
							index = 0;
							
							if(xHigherPriorityTaskWoken)
								portYIELD_FROM_ISR(xHigherPriorityTaskWoken);  
						}
					}
            break;

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}
