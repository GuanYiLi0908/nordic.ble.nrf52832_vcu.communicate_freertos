

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_qwr.h"
#include "nrf_fstorage.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "bsp_btn_ble.h"

#include "Communication_Bluetooth.h"
#include "Communication_UART.h"
#include "Protocol.h"
#include "Protocol_PackageDefine.h"
#include "Timer.h"
#include "TPMS.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define DEVICE_NAME                     "AWAYSPEED"            /**< Name of device used for advertising. */
//#define DEVICE_NAME                     "1j4ul4xu06ji3" 

NRF_BLE_GATT_DEF(m_gatt);                                               /**< GATT module instance. 		 */
BLE_ADVERTISING_DEF(m_advertising);                                 		/**< Advertising module instance. */
NRF_BLE_SCAN_DEF(m_scan);                                               /**< Scanning Module instance. */
NRF_BLE_QWR_DEF(m_qwr);																									/**< Context for the Queued Write module.*/

BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);                       /**< BLE NUS service instance. */

TaskHandle_t N_ManufacSpecDataPackagingFinish = NULL;
MANUFACTURER_DATA_RECEIVE ManuFacDataRx;
BLUETOOTHTASK sBluetoothTask;

extern QueueHandle_t 			Q_BluetoothDataRx;
extern QueueHandle_t 			Q_BluetoothDataTx;
extern QueueHandle_t 			Q_FunctionMode;
extern SemaphoreHandle_t 	Semphr_DiagnosticsTimer;
extern SemaphoreHandle_t 	Semphr_EntryIdleMode;
extern SemaphoreHandle_t 	Semphr_ConncetTime;
extern SemaphoreHandle_t 	Semphr_ProtclTskStructRst;
extern TimerHandle_t 		 	T_ConnectTimeoutTime;

extern BLUETOOTHCONTROLRSP 		sBLECtrlRsp_A9;
extern VCUHARDWARESTATUS	 		sVCUHwStatus_89; 
extern VCUMILEAGEINFORMATION	sVCUMileageErr_8A;
extern VCUBASICINFO						sVCUBasicInfo_8B;
extern DRIVERHARDWARESTATUS		sDrvHwStatus_91;
extern DRIVERBASICINFO				sDrvBasicInfo_92;
extern BMSSYSTEMINFORMATION		sBMSSysInfo_99; 
extern BMSGROUPINFORMATION 		sBMSGroupInfo_9A;	
extern BMSPACKAGEINFORMATION1 sBMSPackageInfo1_9B; 
extern BMSPACKAGEINFORMATION2 sBMSPackageInfo2_9C; 
extern BMSPACKAGEINFORMATION3 sBMSPackageInfo3_9D; 
extern TPMSINFORMATION 				sTPMSinfo_B9; 
extern TPMSMANUFACINFORMATION sTPMSManuFacInfo_BA; 
extern EMERGENCYERRORINFO 		sEmergencyErr_D9;

uint8_t BluetoothConnectionStatus = BLEConnectStatus_Invalid;

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;  			 				/**< Handle of the current connection. */	
static uint8_t m_target_manufacturer_specific_data[NRF_BLE_ADV_MANUFACTURER_SPECIFIC_DATA_LENGTH] = { 
																				0x02, 0x15, 
																				0xB5, 0x4A, 0xDC, 0x00, 0x67, 0xF9, 0x11, 0xD9,
																				0x96, 0x69, 0x08, 0x00, 0x20, 0x0C, 0x9A, 0x66																			
																				};
																				
/**< Scan parameters requested for scanning and connection. */
static ble_gap_scan_params_t m_scan_param =
{
        .active        = 0,           /* Disable the acvtive scanning */
        .interval      = NRF_BLE_SCAN_SCAN_INTERVAL,
        .window        = NRF_BLE_SCAN_SCAN_WINDOW,
        .filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL,
        .timeout       = 0,
        .scan_phys     = BLE_GAP_PHY_1MBPS,
};

/**@brief UUIDs that the central application scans for if the name above is set to an empty string,
 * and that are to be advertised by the peripherals.
 */
static ble_uuid_t m_adv_uuids[] =
{
    {BLE_UUID_NUS_SERVICE, BLE_UUID_TYPE_VENDOR_BEGIN}
};

static ble_advdata_manuf_data_t 	m_adv_manuf_data;
static uint8_t m_manufacturer_info[MANUFACTURER_ADVINFO_LENGTH] =  { 0 };


/******************************************************************************/
/*                 	  	 			BlueTooth Task						                	  	*/
/******************************************************************************/
void BluetoothTask(void *pvParameters)
{
	uint32_t err_code;
	ble_evt_t BluetoothEvent;
	uint16_t Connect_Handler = BluetoothEvent.evt.gap_evt.conn_handle;

	uint8_t res = 0;
	uint8_t tempDiagnosticsTrans_Cnt = 0;
	uint8_t tempDiagnosticsData_Array[PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH] = { 0 };
	uint16_t tempDiagnosticsDatalength = PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH;
	uint8_t BluetoothQueueData_Array[PROTOCOL_COMMAND_PACKAGE_MAXIMUM_LENGTH] = { 0 };
	uint8_t BluetoothASCIIData_Array[200] ={ 0 };
	
	uint8_t StructArray_Cnt = 0;
	uint8_t tempFunctionMode_Buf = 0;
	
//	uint32_t *ProtocolPackageStruct_Array[12];
//	ProtocolPackageStruct_Array[0] = (uint32_t*)&sBMSPackageInfo2_9C;
	void *ProtocolPackageStructAddr_Array[PROTOCOL_DIAGNOSTICS_INFORMATION_TABLE_NUM];
	ProtocolPackageStructAddr_Array[0]  = &sVCUHwStatus_89; 
	ProtocolPackageStructAddr_Array[1]  = &sVCUMileageErr_8A;
	ProtocolPackageStructAddr_Array[2]  = &sVCUBasicInfo_8B;
	ProtocolPackageStructAddr_Array[3]  = &sDrvHwStatus_91;
	ProtocolPackageStructAddr_Array[4]  = &sDrvBasicInfo_92;
	ProtocolPackageStructAddr_Array[5]  = &sBMSSysInfo_99; 
	ProtocolPackageStructAddr_Array[6]  = &sBMSGroupInfo_9A;	
	ProtocolPackageStructAddr_Array[7]  = &sBMSPackageInfo1_9B; 
	ProtocolPackageStructAddr_Array[8]  = &sBMSPackageInfo2_9C; 
	ProtocolPackageStructAddr_Array[9]  = &sBMSPackageInfo3_9D; 
	ProtocolPackageStructAddr_Array[10] = &sTPMSinfo_B9; 
	ProtocolPackageStructAddr_Array[11] = &sTPMSManuFacInfo_BA; 
	ProtocolPackageStructAddr_Array[12] = &sEmergencyErr_D9; 
	
	while(1)
	{
		/* Determine Bluetooth is connected or not */
//		if(ble_conn_state_status(Connect_Handler) == BLE_CONN_STATUS_CONNECTED)
		if(BluetoothConnectionStatus == BLEConnectStatus_Connect)
		{
			switch(sBluetoothTask.eTransProcess)
			{
				case StartConnect:
					if (xTimerStart(T_ConnectTimeoutTime, 2) != pdPASS)
						APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
					sBluetoothTask.eTransProcess = WaitCertification;
				
				case WaitCertification:	
					/* Determine the connect is timeout or not */
					if(Semphr_ConncetTime != NULL)
					{
						BaseType_t err = xSemaphoreTake(Semphr_ConncetTime, 0);
						if(err == pdTRUE)
						{
							NRF_LOG_DEBUG("Certification Timeout, Disconnect");
							Bluetooth_DisconnectConnection();
						
							if(xTimerStop(T_ConnectTimeoutTime, 2) != pdPASS)
								NRF_LOG_DEBUG("xTimerStop Error");

							sBluetoothTask.eTransProcess = StartConnect;
							break;
						}
					}
					
				case ReceivePackage:
					switch(sBluetoothTask.eBLEProcess)
					{
						case BleTask_Certification:
							if(Q_BluetoothDataTx != NULL)
							{
								res = xQueueReceive(Q_BluetoothDataTx, &BluetoothQueueData_Array, 0);
								if(res == pdPASS)
								{
									/* Certification, stop timeout timer */
									if(xTimerStop(T_ConnectTimeoutTime, 2) != pdPASS)
										NRF_LOG_DEBUG("xTimerStop Error");

									sBluetoothTask.eTransProcess = ReceivePackage;
									
#ifdef forBluetoothTxNRF_LOG
									NRF_LOG_DEBUG("BluetoothQueueData_Array :");
									for(uint8_t i = 0; i < BluetoothQueueData_Array[2]; i++)
									{
										NRF_LOG_RAW_INFO(" %02X ", BluetoothQueueData_Array[i]);
									}
									NRF_LOG_RAW_INFO("\r\n");
#endif						
									
									uint8_t ASCIIArrayLength_Buf = Bluetooth_HEX2ASCII(BluetoothQueueData_Array, 
																																		 BluetoothQueueData_Array[2], 
																																		 BluetoothASCIIData_Array);
									
									Bluetooth_TransmitInfoCalcu(&sBluetoothTask, ASCIIArrayLength_Buf);
									sBluetoothTask.eTransProcess = TransmitPackage;
								}
							}
							
							if(Semphr_EntryIdleMode != NULL )
							{
								BaseType_t err = xSemaphoreTake(Semphr_EntryIdleMode, 0);
								if(err == pdTRUE)
								{
									NRF_LOG_DEBUG("Bluetooth Task Change to Idle Mode");
									sBluetoothTask.eBLEProcess = BleTask_Idle;
								}
							}
							break;
							
						case BleTask_Idle:
						{
							if(Q_FunctionMode != NULL)
							{
								res = xQueueReceive(Q_FunctionMode, &tempFunctionMode_Buf, 0);
								if(res == pdPASS)
								{
									switch(tempFunctionMode_Buf)
									{
										case FunctionMode_Diagnostic:
											NRF_LOG_DEBUG("Bluetooth Task Change to Diagnstic Mode");
											sBluetoothTask.eBLEProcess = BleTask_Diagnostic;
											break;
										
										case FunctionMode_RecordTableOperation:
											NRF_LOG_DEBUG("Bluetooth Task Change to Record Table Mode");
											sBluetoothTask.eBLEProcess = BleTask_RecordTable;
											break;
										
										default:
											break;
									}
								}
							}
							
							if(Q_BluetoothDataTx != NULL)
							{
								res = xQueueReceive(Q_BluetoothDataTx, &BluetoothQueueData_Array, 0);
								if(res == pdPASS)
								{
									
#ifdef forBluetoothTxNRF_LOG
									NRF_LOG_DEBUG("BluetoothQueueData_Array :");
									for(uint8_t i = 0; i < BluetoothQueueData_Array[2]; i++)
									{
										NRF_LOG_RAW_INFO(" %02X ", BluetoothQueueData_Array[i]);
									}
									NRF_LOG_RAW_INFO("\r\n");
#endif										
											
									uint8_t ASCIIArrayLength_Buf = Bluetooth_HEX2ASCII(BluetoothQueueData_Array, 
																																		 BluetoothQueueData_Array[2], 
																																		 BluetoothASCIIData_Array);
									
									Bluetooth_TransmitInfoCalcu(&sBluetoothTask, ASCIIArrayLength_Buf);
									sBluetoothTask.eTransProcess = TransmitPackage;									
								}
							}
						} break;
				
						case BleTask_Diagnostic:
						{							
							if(Q_BluetoothDataTx != NULL)
							{
								res = xQueueReceive(Q_BluetoothDataTx, &BluetoothQueueData_Array, 0);
								if(res == pdPASS)
								{
#ifdef forBluetoothTxNRF_LOG
									NRF_LOG_DEBUG("BluetoothQueueData_Array :");
									for(uint8_t i = 0; i < BluetoothQueueData_Array[2]; i++)
									{
										NRF_LOG_RAW_INFO(" %02X ", BluetoothQueueData_Array[i]);
									}
									NRF_LOG_RAW_INFO("\r\n");
#endif											
									
									uint8_t ASCIIArrayLength_Buf = Bluetooth_HEX2ASCII(BluetoothQueueData_Array, 
																																		 BluetoothQueueData_Array[2], 
																																		 BluetoothASCIIData_Array);										
										
									Bluetooth_TransmitInfoCalcu(&sBluetoothTask, ASCIIArrayLength_Buf);
									sBluetoothTask.eTransProcess = TransmitPackage;									
								}
								else
								{
									uint8_t ASCIIArrayLength_Buf = 0;
									if(Semphr_DiagnosticsTimer != NULL)
									{
										BaseType_t err = xSemaphoreTake(Semphr_DiagnosticsTimer, 0);
										if((tempDiagnosticsTrans_Cnt % 7) == 0)
										{
											memcpy(tempDiagnosticsData_Array, 
														 ProtocolPackageStructAddr_Array[StructArray_Cnt], 
														 PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH);
											
											ASCIIArrayLength_Buf = Bluetooth_HEX2ASCII(tempDiagnosticsData_Array,
																																 PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH,
																																 BluetoothASCIIData_Array);
											
											Bluetooth_TransmitInfoCalcu(&sBluetoothTask, ASCIIArrayLength_Buf);
											StructArray_Cnt = StructArray_Cnt + 1;
											if(StructArray_Cnt > PROTOCOL_DIAGNOSTICS_INFORMATION_TABLE_NUM - 1)
											{
												StructArray_Cnt = 0;
												tempDiagnosticsTrans_Cnt = 0;
											}
											sBluetoothTask.eTransProcess = TransmitPackage;
										}
									}
								}
								
								if(Q_FunctionMode != NULL)
								{
									res = xQueueReceive(Q_FunctionMode, &tempFunctionMode_Buf, 0);
									if(res == pdPASS)
									{
										if(tempFunctionMode_Buf == FunctionMode_Idle)
										{
											NRF_LOG_DEBUG("Bluetooth Task Change to Idle Mode");
											sBluetoothTask.eBLEProcess = BleTask_Idle;
										}
									}									
								}
							}
						} break;
						
						case BleTask_RecordTable:
						{
							if(Q_BluetoothDataTx != NULL)
							{
								res = xQueueReceive(Q_BluetoothDataTx, &BluetoothQueueData_Array, 0);
								if(res == pdPASS)
								{
									uint8_t ASCIIArrayLength_Buf = Bluetooth_HEX2ASCII(BluetoothQueueData_Array, 
																																		 BluetoothQueueData_Array[2], 
																																		 BluetoothASCIIData_Array);										
										
									Bluetooth_TransmitInfoCalcu(&sBluetoothTask, ASCIIArrayLength_Buf);
									sBluetoothTask.eTransProcess = TransmitPackage;									
								}
							}
							
							if(Q_FunctionMode != NULL)
							{
								res = xQueueReceive(Q_FunctionMode, &tempFunctionMode_Buf, 0);
								if(res == pdPASS)
								{
									if(tempFunctionMode_Buf == FunctionMode_Idle)
									{
										NRF_LOG_DEBUG("Bluetooth Task Change to Idle Mode");
										sBluetoothTask.eBLEProcess = BleTask_Idle;
									}
								}
							}
						}	break;
					}
					break;
			
				case TransmitPackage:
					if(sBluetoothTask.sTransInfo.TransCycle > 0)
					{
						if(sBluetoothTask.sTransInfo.TxLength >= BLUETOOTH_TRANSMIT_DATA_LENGTH)
						{
							uint8_t *tempBluetoothTxData = pvPortMalloc(BLUETOOTH_TRANSMIT_DATA_LENGTH * sizeof(uint8_t));
							memcpy(tempBluetoothTxData, 
										 BluetoothASCIIData_Array + sBluetoothTask.sTransInfo.TransPosition, 
										 BLUETOOTH_TRANSMIT_DATA_LENGTH);

#ifdef forBluetoothTxNRF_LOG
									NRF_LOG_DEBUG("tempBluetoothTxData :");
									for(uint8_t i = 0; i < BLUETOOTH_TRANSMIT_DATA_LENGTH; i++)
									{
										NRF_LOG_RAW_INFO(" %02X ", tempBluetoothTxData[i]);
									}
									NRF_LOG_RAW_INFO("\r\n");
#endif		
							
							uint16_t TransLen_Buf = BLUETOOTH_TRANSMIT_DATA_LENGTH; 
							do
							{
								 err_code = ble_nus_data_send(&m_nus, tempBluetoothTxData, &TransLen_Buf, m_conn_handle);
								 if((err_code != NRF_ERROR_INVALID_STATE) &&
									  (err_code != NRF_ERROR_RESOURCES) &&
										(err_code != NRF_ERROR_NOT_FOUND))
								 {
									 if(err_code != NRF_SUCCESS)
										sBluetoothTask.eTransProcess = ErrorProcess;
								 }
							}while (err_code == NRF_ERROR_RESOURCES);
							
							vPortFree(tempBluetoothTxData);
							
							sBluetoothTask.sTransInfo.TransPosition = sBluetoothTask.sTransInfo.TransPosition + TransLen_Buf;
							sBluetoothTask.sTransInfo.TxLength = sBluetoothTask.sTransInfo.TxLength - TransLen_Buf;
							sBluetoothTask.sTransInfo.TransCycle = sBluetoothTask.sTransInfo.TransCycle - 1;
						}
						else
						{
							uint8_t *tempBluetoothTxData = pvPortMalloc(sBluetoothTask.sTransInfo.TxLength * sizeof(uint8_t));
							memcpy(tempBluetoothTxData,
										 BluetoothASCIIData_Array + sBluetoothTask.sTransInfo.TransPosition, 
										 sBluetoothTask.sTransInfo.TxLength);

#ifdef forBluetoothTxNRF_LOG
									NRF_LOG_DEBUG("tempBluetoothTxData :");
									for(uint8_t i = 0; i < sBluetoothTask.sTransInfo.TxLength; i++)
									{
										NRF_LOG_RAW_INFO(" %02X ", tempBluetoothTxData[i]);
									}
									NRF_LOG_RAW_INFO("\r\n");
#endif	
							
							uint16_t TransLen_Buf = sBluetoothTask.sTransInfo.TxLength; 
							do
							{
								 err_code = ble_nus_data_send(&m_nus, tempBluetoothTxData, &TransLen_Buf, m_conn_handle);
								 if((err_code != NRF_ERROR_INVALID_STATE) &&
									  (err_code != NRF_ERROR_RESOURCES) &&
										(err_code != NRF_ERROR_NOT_FOUND))
								 {
									 if(err_code != NRF_SUCCESS)
										sBluetoothTask.eTransProcess = ErrorProcess;
								 }
							}while (err_code == NRF_ERROR_RESOURCES);
									
							vPortFree(tempBluetoothTxData);
							sBluetoothTask.sTransInfo.TransPosition = sBluetoothTask.sTransInfo.TransPosition + TransLen_Buf;
							sBluetoothTask.sTransInfo.TxLength = 0;
							sBluetoothTask.sTransInfo.TransCycle = sBluetoothTask.sTransInfo.TransCycle - 1;
						}
//						vTaskDelay(10);
					}
					else
					{
						memset(BluetoothASCIIData_Array, 0 ,(sizeof(BluetoothASCIIData_Array) / sizeof(BluetoothASCIIData_Array[0])));
						memset(&sBluetoothTask.sTransInfo, 0, sizeof(sBluetoothTask.sTransInfo));
						sBluetoothTask.eTransProcess = ReceivePackage;
					}
					break;
					
				case ErrorProcess:
					NRF_LOG_DEBUG("ble_nus_data_send Error: %X", err_code);
					APP_ERROR_CHECK(err_code);
					Bluetooth_DisconnectConnection();
					break;
			}
		}
		
		if(BluetoothConnectionStatus == BLEConnectStatus_Disconnect)
		{
			NRF_LOG_DEBUG("Reset Whole sBluetoothTask Struct");
			if(Semphr_ProtclTskStructRst != NULL)
			{
				BaseType_t err = xSemaphoreTake(Semphr_ProtclTskStructRst, 0);
				if(err == pdTRUE)
				{
					BluetoothConnectionStatus = BLEConnectStatus_Invalid;
					memset(&sBluetoothTask, 0, sizeof(sBluetoothTask)); 
				}
			}
		}
		vTaskDelay(1);
	}
}

static uint8_t Bluetooth_HEX2ASCII(uint8_t *InputArray, uint8_t InputArrayLength, uint8_t *OutputArray)
{
	uint8_t OutputArrayLength_Buf = (InputArrayLength * 2) + 2;
	uint8_t HEX2ASCII_Array[2] = { 0 };
	uint8_t OutputArrayPosition_Buf = 0;
	
	for(uint8_t i = 0; i < InputArrayLength; i++)
	{
		uint8_t tempData_Buf = InputArray[i];
		HEX2ASCII_Array[0] = (tempData_Buf & 0xF0) >> 4;
		HEX2ASCII_Array[1] = (tempData_Buf & 0x0F);
		for(uint8_t y = 0; y < 2; y++)
		{
			if(HEX2ASCII_Array[y] <= 9)
			{
				HEX2ASCII_Array[y] = HEX2ASCII_Array[y] + '0';
			}
			else
			{
				HEX2ASCII_Array[y] = HEX2ASCII_Array[y] + 0x37;
			}
		}
		OutputArray[OutputArrayPosition_Buf] = HEX2ASCII_Array[0];
		OutputArray[OutputArrayPosition_Buf + 1] = HEX2ASCII_Array[1];
		OutputArrayPosition_Buf = OutputArrayPosition_Buf + 2;
	}
	
	OutputArray[OutputArrayLength_Buf - 1] = 0x0D;
	OutputArray[OutputArrayLength_Buf - 2] = 0xFF;
	
#ifdef forASCIIResultNRF_LOG
	NRF_LOG_DEBUG("ASCII OutputArray :");
	for(uint8_t i = 0; i < OutputArrayLength_Buf; i++)
	{
		NRF_LOG_RAW_INFO(" %02X ", OutputArray[i]);
	}
	NRF_LOG_RAW_INFO("\r\n");
#endif
	
	return OutputArrayLength_Buf;
}

static void Bluetooth_TransmitInfoCalcu(BLUETOOTHTASK *p_Task, uint8_t PackageLength)
{
	p_Task -> sTransInfo.TxLength = PackageLength;	
	p_Task -> sTransInfo.TransCycle = p_Task -> sTransInfo.TxLength / BLUETOOTH_TRANSMIT_DATA_LENGTH;
	if((p_Task -> sTransInfo.TxLength % BLUETOOTH_TRANSMIT_DATA_LENGTH) != 0)
		p_Task -> sTransInfo.TransCycle = p_Task -> sTransInfo.TransCycle + 1;
	
#ifdef forBluetoothTxNRF_LOG	
	NRF_LOG_DEBUG("sTransInfo.TxLength : %d", p_Task -> sTransInfo.TxLength);
	NRF_LOG_DEBUG("sTransInfo.TransCycle : %d", p_Task -> sTransInfo.TransCycle);
#endif
}

void Bluetooth_DisconnectConnection(void)
{
	uint32_t err_code;
	
	err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
	APP_ERROR_CHECK(err_code);
	
	BluetoothConnectionStatus = BLEConnectStatus_Disconnect;
}

uint32_t Fble_nus_data_send(uint8_t * p_data, uint16_t * p_length)
{
    uint32_t err_code = ble_nus_data_send(&m_nus, p_data, p_length, m_conn_handle);
    return err_code;
}

/******************************************************************************/
/*                 	  	 	 GAP Function subroutine  		                	  	*/
/******************************************************************************/
/**@brief Function for initializing the GAP.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device, including the device name, appearance, and the preferred connection parameters.
 */
void GAP_Initialization(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = m_scan.conn_params.min_conn_interval;
    gap_conn_params.max_conn_interval = m_scan.conn_params.max_conn_interval;
    gap_conn_params.slave_latency     = m_scan.conn_params.slave_latency;
    gap_conn_params.conn_sup_timeout  = m_scan.conn_params.conn_sup_timeout;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

/******************************************************************************/
/*                 	  	 	 GATT Function subroutine 		                	  	*/
/******************************************************************************/
/**@brief Function for initializing the GATT library. */
void GATT_Initialization(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}

/******************************************************************************/
/*                 	  	 Bluetooth Function subroutine	                	  	*/
/******************************************************************************/
/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupts.
 */
void BLE_ProtocolStack_Initialization(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, BLEEvent_handler, NULL);
}

/**@brief Function for initializing the Connection Parameters module.
 */
void Connect_Initialization(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_CONN_HANDLE_INVALID; // Start upon connection.
    cp_init.disconnect_on_fail             = true;
    cp_init.evt_handler                    = NULL;  // Ignore events.
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the advertising functionality.
 */
void Advertising_Initialization(void)
{
    ret_code_t             		err_code;
		ble_gap_addr_t 						GAP_Addr;
    ble_advertising_init_t 		init;
		
		/* Get Device MAC Address */
		err_code = sd_ble_gap_addr_get(&GAP_Addr);
		if(err_code != NRF_SUCCESS)
			NRF_LOG_ERROR("sd_ble_gap_addr_get Fall !!!");

		memcpy(m_manufacturer_info, GAP_Addr.addr, 6);
		
		NRF_LOG_DEBUG("Device MAC Address : %02X %02X %02X %02X %02X %02X",	GAP_Addr.addr[0],
																																				GAP_Addr.addr[1],
																																				GAP_Addr.addr[2],
																																				GAP_Addr.addr[3],
																																				GAP_Addr.addr[4],
																																				GAP_Addr.addr[5]);
    
		memset(&init, 0, sizeof(init));
		
    init.advdata.include_appearance      = false;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
		
		/* Import Manufacturer Specification Data */
		m_adv_manuf_data.company_identifier 	 = MANUFACTURER_COMPANY_IDENTIFIER;
		m_adv_manuf_data.data.p_data 			 		 = (uint8_t *) m_manufacturer_info;
		m_adv_manuf_data.data.size		 		   	 = MANUFACTURER_ADVINFO_LENGTH;		
		init.advdata.p_manuf_specific_data   = &m_adv_manuf_data;
		
		/* Setting Scan Response include Device Name */
		init.srdata.name_type = BLE_ADVDATA_FULL_NAME;
	
		init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;
		
		init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);	
}

/**@brief Function for initializing the scanning and setting the filters.
 */
void BLE_Scan_Initialization(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));

		init_scan.p_scan_param     = &m_scan_param;
    init_scan.connect_if_match = false;
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, ScanEvent_handler);
    APP_ERROR_CHECK(err_code);

		err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_MANUFACTURER_FILTER, m_target_manufacturer_specific_data);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filters_enable(&m_scan, NRF_BLE_SCAN_MANUFACTURER_SPECIFIC_FILTER, false);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting scanning. */
void BLE_AdvertisingAndScanning_Start(void * p_erase_bonds)
{
  ret_code_t err_code;
	bool erase_bonds = *(bool*)p_erase_bonds;
	
	if(erase_bonds)
	{
		// Advertising is started by PM_EVT_PEERS_DELETE_SUCCEEDED event.
	}
	else
	{
		/* check if there are no flash operations in progress */
		if (!nrf_fstorage_is_busy(NULL))
		{
			/* Start scanning for peripherals and initiate connection to devices which
				advertise Heart Rate or Running speed and cadence UUIDs. */
			err_code = nrf_ble_scan_start(&m_scan);
			APP_ERROR_CHECK(err_code);
	
			/* Start advertising. */
			err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
			APP_ERROR_CHECK(err_code);

			err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
			APP_ERROR_CHECK(err_code);
		}
	}
}

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void BLEEvent_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t            err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected");

            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
						err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
						APP_ERROR_CHECK(err_code);
				
						/* LED indication status change */
						err_code = bsp_indication_set(BSP_INDICATE_SCANNING);
						APP_ERROR_CHECK(err_code);
				
						bsp_board_led_on(BSP_BOARD_LED_1);
				
						BluetoothConnectionStatus = BLEConnectStatus_Connect;
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected");
        
						/* LED indication status change */
						bsp_board_led_on(BSP_BOARD_LED_1);
				
						err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
						APP_ERROR_CHECK(err_code);
				
						BluetoothConnectionStatus = BLEConnectStatus_Disconnect;
				
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

				case BLE_GATTS_EVT_HVN_TX_COMPLETE:
					break;
				
        default:
            // No implementation needed.
            break;
    }
}

static void ScanEvent_handler(scan_evt_t const * p_scan_evt)
{
		ble_gap_evt_adv_report_t const * p_adv_report = p_scan_evt->params.filter_match.p_adv_report;
	
		uint8_t DeviceAdverAddr_Array[BLE_GAP_ADDR_LEN] = { 0 };
		uint8_t ManufacSpecMajorMinor_Array[MANUFACTURER_SPECIFICATION_MAJORMINOR_LENGTH] = { 0 };
		
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
    switch(p_scan_evt->scan_evt_id)
    {
			case NRF_BLE_SCAN_EVT_FILTER_MATCH:
				NRF_LOG_DEBUG("NRF_BLE_SCAN_EVT_FILTER_MATCH");
				
				/* Advertising Address */ 
				memcpy(DeviceAdverAddr_Array, &p_adv_report -> peer_addr.addr, BLE_GAP_ADDR_LEN);
//				NRF_LOG_RAW_INFO("Advertising Address : %02X %02X %02X %02X %02X %02X \r\n", DeviceAdverAddr_Array[0], 
//																																										 DeviceAdverAddr_Array[1],
//				 																																						 DeviceAdverAddr_Array[2],
//																																										 DeviceAdverAddr_Array[3],
//																																										 DeviceAdverAddr_Array[4],
//																																										 DeviceAdverAddr_Array[5]);
				
				/* Beacon Major & Minor */
				memcpy(ManufacSpecMajorMinor_Array, m_scan.scan_buffer.p_data + 
																						NRF_BLE_ADV_MANUFACTURER_SPECIFIC_DATA_LENGTH +
																						ManufacSpecPayload_NoUseOffset,
																						MANUFACTURER_SPECIFICATION_MAJORMINOR_LENGTH);
			
				/* Loading Data */
				ManuFacDataRx.DeviceID[0] = DeviceAdverAddr_Array[0];
				ManuFacDataRx.DeviceID[1] = (ManufacSpecMajorMinor_Array[2] & 0x80) >> 7;
				ManuFacDataRx.DeviceID[2] = ManufacSpecMajorMinor_Array[0];
				ManuFacDataRx.DeviceID[3] = ManufacSpecMajorMinor_Array[1];
				
				ManuFacDataRx.Temperature = ManufacSpecMajorMinor_Array[2] & 0x7F;
				ManuFacDataRx.Pressure		= ManufacSpecMajorMinor_Array[3];
		
#ifdef forScanDataNRF_LOG		
				NRF_LOG_RAW_INFO("Device ID : %02X %02X %02X %02X \r\n", ManuFacDataRx.DeviceID[0],
																																 ManuFacDataRx.DeviceID[1],
																																 ManuFacDataRx.DeviceID[2],
																																 ManuFacDataRx.DeviceID[3]);				
				NRF_LOG_RAW_INFO("Temperature : %02X \r\n", ManuFacDataRx.Temperature);
				NRF_LOG_RAW_INFO("Pressure : %02X \r\n", ManuFacDataRx.Pressure);	
				NRF_LOG_RAW_INFO ("==================================\r\n");
#endif
				
				/* Notify packaging is finish */
				vTaskNotifyGiveFromISR(N_ManufacSpecDataPackagingFinish, &xHigherPriorityTaskWoken);
				
				if(xHigherPriorityTaskWoken)
					portYIELD_FROM_ISR (xHigherPriorityTaskWoken);
				
				break;
			
			default:
				break;
				
    }
}

/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for handling advertising events.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
        {
            NRF_LOG_INFO("Fast advertising.");
        } break;

        case BLE_ADV_EVT_IDLE:
        {
            ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
            APP_ERROR_CHECK(err_code);
        } break;

        default:
            // No implementation needed.
            break;
    }
}

/******************************************************************************/
/*            Nordic UART Service(NUS) preipheral side subroutine        	  	*/
/******************************************************************************/
/**
 *@brief Function for initializing services that will be used by the application.
 **/
void BluetoothServices_Initialization(void)
{
    uint32_t           err_code;
    ble_nus_init_t     nus_init;
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    // Initialize NUS.
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = NUS_Data_handler;

    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_evt       Nordic UART Service event.
 */
/**@snippet [Handling the data received over BLE] */
static void NUS_Data_handler(ble_nus_evt_t * p_evt)
{
	BaseType_t xHigherPriorityTaskWoken;
	BaseType_t err;
	static uint8_t BluetoothRxData_Array[PROTOCOL_COMMAND_PACKAGE_MAXIMUM_LENGTH] = { 0 };
	
	switch(p_evt -> type)
	{
		case BLE_NUS_EVT_RX_DATA:
			memcpy(BluetoothRxData_Array, p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);
		
#ifdef forBluetoothRxNRF_LOG
			NRF_LOG_DEBUG("BluetoothRxData_Array :");
			for(uint8_t i = 0; i < p_evt->params.rx_data.length; i++)
			{
				NRF_LOG_RAW_INFO(" %02X ", BluetoothRxData_Array[i]);
			}
			NRF_LOG_RAW_INFO("\r\n");
#endif
			
			for(uint8_t i = 0; i < p_evt -> params.rx_data.length; i++)
			{
				if(Q_BluetoothDataRx != NULL)
				{
					err = xQueueSendFromISR(Q_BluetoothDataRx, &BluetoothRxData_Array[i], &xHigherPriorityTaskWoken);
					if(err == errQUEUE_FULL)
					{
						NRF_LOG_ERROR("Q_BluetoothDataRx if Full");
					}
					if(xHigherPriorityTaskWoken)
						portYIELD_FROM_ISR(xHigherPriorityTaskWoken); 
				}
			}

			memset(BluetoothRxData_Array, 0, PROTOCOL_COMMAND_PACKAGE_MAXIMUM_LENGTH);
			break;
		
		case BLE_NUS_EVT_TX_RDY:
			break;
		
		case BLE_NUS_EVT_COMM_STARTED:
			break;
		
		case BLE_NUS_EVT_COMM_STOPPED:
			break;
	}
}
