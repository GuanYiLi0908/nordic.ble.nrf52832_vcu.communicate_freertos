

#include <stdint.h>
#include "bsp_btn_ble.h"

#include "Timer.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

TimerHandle_t T_AliveTime;
TimerHandle_t T_DiagnosticsTxBasicTime;
TimerHandle_t T_ConnectTimeoutTime;

extern SemaphoreHandle_t Semphr_ConncetTime;
extern SemaphoreHandle_t Semphr_DiagnosticsTimer;

uint32_t Timer_ConnectBasicTime_Buf = 0;

/******************************************************************************/
/*                 	  			Timer Function subroutine		                	  	*/
/******************************************************************************/
void xTimer_Initialzation(void)
{	
	T_AliveTime = xTimerCreate("AliveTime",
															TIMEINTERVAL_SYSTEM_ALIVE,
															pdTRUE,
															NULL,
															Timer_SystemAlive_Handle);
	if(T_AliveTime == NULL)
	{
		NRF_LOG_ERROR("T_AliveTime xTimerCreate Fail");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	
	T_DiagnosticsTxBasicTime = xTimerCreate("BluetoothTxBasicTime",
																					 TIMEINTERVAL_BASIC_DIAGNOSTICS_TX,
																					 pdTRUE,
																					 NULL,
																					 Timer_DiagnosticsTx_Handle);
	if(T_DiagnosticsTxBasicTime == NULL)
	{
		NRF_LOG_ERROR("T_DiagnosticsTxBasicTime xTimerCreate Fail");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}	
	
	T_ConnectTimeoutTime = xTimerCreate("ConnectTimeoutTime",
																			 TIMEINTERVAL_CONNECT_TIMEOUT,
																			 pdFALSE,
																			 NULL,
																			 Timer_ConnectTimeout_Handle);
	if(T_ConnectTimeoutTime == NULL)
	{
		NRF_LOG_ERROR("T_ConnectTimeoutTime xTimerCreate Fail");
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}	
}			

void xTimer_Start(void)
{	
	if (pdPASS != xTimerStart(T_AliveTime, 2))
  {
    APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
	
	if (pdPASS != xTimerStart(T_DiagnosticsTxBasicTime, 2))
  {
    APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
}

/**
 * @brief Handler for timer events.
 */
static void Timer_SystemAlive_Handle(TimerHandle_t xTimer)
{
	bsp_board_led_invert(BSP_BOARD_LED_3);
}

static void Timer_DiagnosticsTx_Handle(TimerHandle_t xTimer)
{
	xSemaphoreGive(Semphr_DiagnosticsTimer);
}

static void Timer_ConnectTimeout_Handle(TimerHandle_t xTimer)
{
	xSemaphoreGive(Semphr_ConncetTime);
}
