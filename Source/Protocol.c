
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "Protocol.h"
#include "Process_ErrorDefine.h"
#include "hmac-sha256.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

extern QueueHandle_t Q_BluetoothDataRx;
extern QueueHandle_t Q_BluetoothDataTx;
extern QueueHandle_t Q_FunctionMode;
extern SemaphoreHandle_t 	Semphr_EntryIdleMode;
extern SemaphoreHandle_t 	Semphr_ProtclTskStructRst;

extern CELLPHONECONTROLREQ	sPhoneCtrlReq_31;
extern CELLPHONECONTROLRSP	sPhoneCtrlRsp_B1;
extern BLUETOOTHCONTROLRSP	sBLECtrlRsp_A9;

PROTOCOLTASK sProtocolTask;

extern uint8_t BluetoothConnectionStatus;

static uint8_t seed[32] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
														0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
														0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30,
														0x31, 0x32};
static uint8_t key[6] = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB };
static uint8_t hash[32] = { 0 };
/******************************************************************************/
/*                 	  	 				Protocol Task						                	  	*/
/******************************************************************************/

////	hmac_sha256 (hash, // output
////                 seed, // the seed received from bridge
////                  32,
////                 key, // The authentication key
////                 6);
//	
////								NRF_LOG_DEBUG("hash :");
////								for(uint8_t i = 0; i < 32; i++)
////								{
////									NRF_LOG_RAW_INFO(" %02X ", hash[i]);
////								}
////								NRF_LOG_RAW_INFO("\r\n");	
//	

void ProtocolTask(void *pvParameters)
{	
	BaseType_t err;
	
	uint8_t tempFunctionMode_Buf = 0;
	
	while(1)
	{
		if(BluetoothConnectionStatus == BLEConnectStatus_Connect)
		{
			/* Package Determine */
			sProtocolTask.sPackageRes.Header = Protocol_PackageHeaderDetermine(&sProtocolTask);
			switch(sProtocolTask.sPackageRes.Header)
			{
				case PROCESS_SUCCESS:
					sProtocolTask.sPackageRes.Category = Protocol_PackageCategoryDetermine(&sProtocolTask);
					switch(sProtocolTask.sPackageRes.Category)
					{					
						case PROCESS_SUCCESS:
							sProtocolTask.sPackageRes.Composite = PROCESS_SUCCESS;
							break;
					
						case PROCESS_ERR_RSP_KEY_ANS:
							NRF_LOG_DEBUG("PROCESS_ERR_RSP_KEY_ANS");
							sProtocolTask.sPackageRes.Composite = PROCESS_ERR_RSP_KEY_ANS;
							break;
					
						case PROCESS_ERR_CTRL_ITEM:
							NRF_LOG_DEBUG("PROCESS_ERR_CTRL_ITEM");
							sProtocolTask.sPackageRes.Composite = PROCESS_ERR_CTRL_ITEM;
							break;
					
						default:
							break;
					}
					break;
				
				case PROCESS_ERR_UNKNOW_COMMAND:
					NRF_LOG_DEBUG("PROCESS_ERR_UNKNOW_COMMAND");
					sProtocolTask.sPackageRes.Composite = PROCESS_ERR_UNKNOW_COMMAND;
					break;

				case PROCESS_ERR_LENGTH_OUTRANGE:
					NRF_LOG_DEBUG("PROCESS_ERR_LENGTH_OUTRANGE");
					sProtocolTask.sPackageRes.Composite = PROCESS_ERR_LENGTH_OUTRANGE;
					break;
			
				default:
					break;
			}
		
			/* Protocol Process */
			if(sProtocolTask.sPackageRes.Composite == PROCESS_SUCCESS)
			{
				switch(sProtocolTask.eMainProcess)
				{
					case ProtclTask_Certification:
						switch(sProtocolTask.eCertifiProcess)
						{
							case Certifi_SeedReq:
								if(sProtocolTask.sPackageRes.CommCgy == PhoneCtrlReq_Seed)
								{
									NRF_LOG_DEBUG("cPhoneCtrl_Req -> SeedReq");
									
									/* Rsp_SendSeed */
									Protocol_ResponsePackageTrans(Rsp_SendSeed);
									sProtocolTask.eCertifiProcess = Certifi_KeyAnsRsp;
								}
								else
								{
									NRF_LOG_DEBUG("PROCESS_ERR_WRONG_COMMAND");
									sProtocolTask.sPackageRes.Composite = PROCESS_ERR_WRONG_COMMAND;
								}
								break;
								
							case Certifi_KeyAnsRsp:
								if(sProtocolTask.sPackageRes.CommCgy == PhoneCtrlRsp_KeyAnswer)
								{
									NRF_LOG_DEBUG("cPhoneCtrl_Req -> RspKeyAns");
									Protocol_ResponsePackageTrans(Rsp_AnsSuccess);
									
									/* Entry to the Idle Mode */
									NRF_LOG_DEBUG("Protocol Task Change to Idle Mode");
									sProtocolTask.eMainProcess = ProtclTask_Idle;
									
									/* Notify the Bluetooth Task to change mode */
									xSemaphoreGive(Semphr_EntryIdleMode);
									
									/* Back to default state */
									sProtocolTask.eCertifiProcess = Certifi_SeedReq;
								}
								else
								{
									NRF_LOG_DEBUG("PROCESS_ERR_WRONG_COMMAND");
									sProtocolTask.sPackageRes.Composite = PROCESS_ERR_WRONG_COMMAND;
								}
								break;
						}
						break;
						
					case ProtclTask_Idle:
					{					
						switch(sProtocolTask.sPackageRes.CommCgy)
						{
							case PhoneCtrlReq_setDiagTransStart:
								NRF_LOG_DEBUG("cPhoneCtrl_Req -> setTransStart");
								
								/* Rsp_TransStart */
								Protocol_ResponsePackageTrans(Rsp_DiagTransStart);
							
								sProtocolTask.eMainProcess = ProtclTask_Diagnostic;
								tempFunctionMode_Buf = FunctionMode_Diagnostic;
								break;
							
							case PhoneCtrlReq_RecordTablelen:
								NRF_LOG_DEBUG("cPhoneCtrl_Req -> RecordTableLength");
							
								/* Ask to VCU */
							
								/* Answer phone */
								Protocol_ResponsePackageTrans(Rsp_RecordTableLen);
							
								/* Ask success */
								sProtocolTask.eMainProcess = ProtclTask_RecordTableOperation;
								tempFunctionMode_Buf = FunctionMode_RecordTableOperation;	
								break;
							
							default:
								NRF_LOG_DEBUG("PROCESS_ERR_WRONG_COMMAND");
								sProtocolTask.sPackageRes.Composite = PROCESS_ERR_WRONG_COMMAND;
								break;
						}
						
						if(Q_FunctionMode != NULL)
						{
							err = xQueueGenericSend(Q_FunctionMode, &tempFunctionMode_Buf, 0, queueOVERWRITE);
							if(err == errQUEUE_FULL)
								NRF_LOG_ERROR("Q_FunctionMode if Full");
						}
					} break;
						
					case ProtclTask_Diagnostic:
						if(sProtocolTask.sPackageRes.CommCgy == PhoneCtrlReq_setDiagTransSTOP)
						{
							NRF_LOG_DEBUG("cPhoneCtrl_Req -> setTransSTOP");
							
							/* Rsp_TransSTOP */
							Protocol_ResponsePackageTrans(Rsp_DiagTransSTOP);
							
							/* Back to the Idle Mode */
							NRF_LOG_DEBUG("Protocol Task Change to Idle Mode");
							sProtocolTask.eMainProcess = ProtclTask_Idle;
							tempFunctionMode_Buf = FunctionMode_Idle;
						}
						else
						{
							NRF_LOG_DEBUG("PROCESS_ERR_WRONG_COMMAND");
							sProtocolTask.sPackageRes.Composite = PROCESS_ERR_WRONG_COMMAND;
						}
						
						if(Q_FunctionMode != NULL)
						{
							err = xQueueGenericSend(Q_FunctionMode, &tempFunctionMode_Buf, 0, queueOVERWRITE);
							if(err == errQUEUE_FULL)
								NRF_LOG_ERROR("Q_FunctionMode if Full");
						}
						break;
						
					case ProtclTask_RecordTableOperation:
						switch(sProtocolTask.sPackageRes.CommCgy)
						{
							case PhoneCtrlReq_RecordTableData:
								NRF_LOG_DEBUG("cPhoneCtrl_Req -> RecordTableData");
								Protocol_ResponsePackageTrans(Rsp_RecordTableData);
								break;
							
							case PhoneCtrlReq_RecordTableStop:
								NRF_LOG_DEBUG("cPhoneCtrl_Req -> RecordTableStop");
							
								/* Rsp_RecordTableStop */
								Protocol_ResponsePackageTrans(Rsp_RecordTableStop);
							
								/* Back to the Idle Mode */
								NRF_LOG_DEBUG("Protocol Task Change to Idle Mode");
								sProtocolTask.eMainProcess = ProtclTask_Idle;
								tempFunctionMode_Buf = FunctionMode_Idle;	
								break;
							
							default:
								NRF_LOG_DEBUG("PROCESS_ERR_WRONG_COMMAND");
								sProtocolTask.sPackageRes.Composite = PROCESS_ERR_WRONG_COMMAND;
								break;
						}
						
						if(Q_FunctionMode != NULL)
						{
							err = xQueueGenericSend(Q_FunctionMode, &tempFunctionMode_Buf, 0, queueOVERWRITE);
							if(err == errQUEUE_FULL)
								NRF_LOG_ERROR("Q_FunctionMode if Full");
						}
						break;
				}
			}
			
			/* Error Process*/
			switch(sProtocolTask.sPackageRes.Composite)
			{
				case PROCESS_UNWORKING:
					break;
				
				case PROCESS_SUCCESS:
					/* Clear Determine Result */
					sProtocolTask.sPackageRes.Header			= PROCESS_UNWORKING;
					sProtocolTask.sPackageRes.Category		= PROCESS_UNWORKING;
					sProtocolTask.sPackageRes.Composite		= PROCESS_UNWORKING;
					sProtocolTask.sPackageRes.CommCgy			= 0;
					break;
				
				case PROCESS_ERR_UNKNOW_COMMAND:
				case PROCESS_ERR_LENGTH_OUTRANGE:
				case PROCESS_ERR_CTRL_ITEM:
				case PROCESS_ERR_WRONG_COMMAND:
					NRF_LOG_DEBUG("cBLECtrl_Rsp -> Rejected");
					Protocol_ResponsePackageTrans(Rsp_Rejected);
				
					/* Back to default state */
					sProtocolTask.eCertifiProcess = Certifi_SeedReq;
					sProtocolTask.eMainProcess = ProtclTask_Certification;
					memset(&sProtocolTask.sPackageRes, 0, sizeof(sProtocolTask.sPackageRes));
				
					/* Reset whole struct */
					Bluetooth_DisconnectConnection();
					break;
						
				case PROCESS_ERR_RSP_KEY_ANS:
					NRF_LOG_DEBUG("cBLECtrl_Rsp -> AnsFailure");
					Protocol_ResponsePackageTrans(Rsp_AnsFailure);
				
					/* Back to default state */
					sProtocolTask.eCertifiProcess = Certifi_SeedReq;
					sProtocolTask.eMainProcess = ProtclTask_Certification;
					memset(&sProtocolTask.sPackageRes, 0, sizeof(sProtocolTask.sPackageRes));
				
					/* Disconnect */
					Bluetooth_DisconnectConnection();
					break;
				
				default:
					break;
			}
		}
		
		if(BluetoothConnectionStatus == BLEConnectStatus_Disconnect)
		{
			NRF_LOG_DEBUG("Reset Whole sProtocolTask Struct");
			memset(&sProtocolTask, 0, sizeof(sProtocolTask));
			xSemaphoreGive(Semphr_ProtclTskStructRst);
		}
		vTaskDelay(1);
	}
}

static uint8_t Protocol_PackageHeaderDetermine(PROTOCOLTASK *p_Task)
{
	BaseType_t err;
	uint8_t result = PROCESS_UNWORKING;

	switch(p_Task -> eHeaderProcess)
	{
		case Package_Header:
			if(Q_BluetoothDataRx != NULL)
			{
				err = xQueueReceive(Q_BluetoothDataRx, &p_Task -> sPackage.OBDHeader, 0);
				if(err == pdPASS)
				{
//					NRF_LOG_DEBUG("sPackageHeader.OBDHeader : %X", p_Task -> sPackage.OBDHeader);
					if(p_Task -> sPackage.OBDHeader == ohCellPhoneApp)
					{
						p_Task -> eHeaderProcess = Package_CategoryID;
					}
					else
						p_Task -> eHeaderProcess = Package_Header;
				}
			}
			break;
		
		case Package_CategoryID:
			err = xQueueReceive(Q_BluetoothDataRx, &p_Task -> sPackage.CategoryID, 0);
			if(err == pdPASS)
			{
//				NRF_LOG_DEBUG("sPackageHeader.CategoryID : %X", p_Task -> sPackage.CategoryID);
				if((p_Task -> sPackage.CategoryID == cPhoneCtrl_Req) ||
					 (p_Task -> sPackage.CategoryID == cPhoneCtrl_Rsp))
				{
					p_Task -> eHeaderProcess = Package_Length;
				}
				else
				{	
					result = PROCESS_ERR_UNKNOW_COMMAND;
					p_Task -> eHeaderProcess = Package_Header;
				}
			}
			else
			{
				NRF_LOG_DEBUG("Protocol_PackageCategoryID Receive Error");
				p_Task -> eHeaderProcess = Package_Header;
			}
			break;
		
		case Package_Length:
			err = xQueueReceive(Q_BluetoothDataRx, &p_Task -> sPackage.Length, 0);
			if(err == pdPASS)
			{
//				NRF_LOG_DEBUG("sPackageHeader.Length : %d", p_Task -> sPackage.Length);
				if((p_Task -> sPackage.Length >= PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH) &&
					 (p_Task -> sPackage.Length <= PROTOCOL_COMMAND_PACKAGE_MAXIMUM_LENGTH))
				{
					p_Task -> eHeaderProcess = Package_Data;
				}
				else
				{	
					result = PROCESS_ERR_LENGTH_OUTRANGE;
					p_Task -> eHeaderProcess = Package_Header;
				}
			}
			else
			{
				NRF_LOG_DEBUG("Protocol_PackageLength Receive Error");
				p_Task -> eHeaderProcess = Package_Header;
			}
			break;
			
		case Package_Data:
		{
			uint8_t tempData_Buf = 0;
			err = xQueueReceive(Q_BluetoothDataRx, &tempData_Buf, portMAX_DELAY);
			if(err == pdPASS)
			{
				p_Task -> sPackage.Data[p_Task -> PackageLengthCounter] = tempData_Buf;
				p_Task -> PackageLengthCounter = p_Task -> PackageLengthCounter + 1;
				
				if((p_Task->sPackage.Length - PROTOCOL_PACKAGE_HEADER_LENGTH) == p_Task -> PackageLengthCounter)
				{
					p_Task->PackageLengthCounter = 0;
					
#ifdef forProtocolNRF_LOG
					NRF_LOG_DEBUG("sPackageHeader.Data :");
					for(uint8_t i = 0; i < p_Task -> sPackage.Length; i++)
					{
						NRF_LOG_RAW_INFO(" %02X ", p_Task -> sPackage.Data[i]);
					}
					NRF_LOG_RAW_INFO("\r\n");									
#endif						
					result = PROCESS_SUCCESS;
					p_Task -> eHeaderProcess = Package_Header;
				}
			}
		}
			break;
	}
	return result;
}

static uint8_t Protocol_PackageCategoryDetermine(PROTOCOLTASK *p_Task)
{
	BaseType_t err;
	uint8_t result = PROCESS_UNWORKING;
	
	switch(p_Task -> sPackage.CategoryID)
	{
		case cPhoneCtrl_Req:
		{
			uint8_t *tempPackageData_Array = pvPortMalloc(p_Task -> sPackage.Length * sizeof(uint8_t));
			
			tempPackageData_Array[0] = p_Task -> sPackage.OBDHeader;
			tempPackageData_Array[1] = p_Task -> sPackage.CategoryID;
			tempPackageData_Array[2] = p_Task -> sPackage.Length;
			
			memcpy(tempPackageData_Array + PROTOCOL_PACKAGE_HEADER_LENGTH, 
						 &p_Task -> sPackage.Data,
						 p_Task -> sPackage.Length - PROTOCOL_PACKAGE_HEADER_LENGTH);
			
#ifdef forProtocolNRF_LOG
			NRF_LOG_DEBUG("tempPackageData_Array :");
			for(uint8_t i = 0; i < p_Task -> sPackage.Length; i++)
			{
				NRF_LOG_RAW_INFO(" %02X ", tempPackageData_Array[i]);
			}
			NRF_LOG_RAW_INFO("\r\n");									
#endif	
			
			memcpy(&sPhoneCtrlReq_31, tempPackageData_Array, PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH);		
			if(((sPhoneCtrlReq_31.AccountAccess != 0) ^ 
					(sPhoneCtrlReq_31.DiagTransCtrl != 0) ^ 
					(sPhoneCtrlReq_31.ExtenPatloadTransCtrl != 0) ^
					(sPhoneCtrlReq_31.RegenModeCtrl != 0)) == true )
			{
				switch(sPhoneCtrlReq_31.AccountAccess)
				{
					case UnusedField:
						break;
					
					case  SeedReq:
						p_Task -> sPackageRes.CommCgy = PhoneCtrlReq_Seed;
						result = PROCESS_SUCCESS;
						break;
				}
				
				switch(sPhoneCtrlReq_31.DiagTransCtrl)
				{
					case UnusedField:
						break;

					case setDiagTransStart:
						p_Task -> sPackageRes.CommCgy = PhoneCtrlReq_setDiagTransStart;
						result = PROCESS_SUCCESS;
						break;
					
					case setDiagTransSTOP:
						p_Task -> sPackageRes.CommCgy = PhoneCtrlReq_setDiagTransSTOP;
						result = PROCESS_SUCCESS;
						break;
				}
				
				switch(sPhoneCtrlReq_31.ExtenPatloadTransCtrl)
				{
					case UnusedField:
						break;
					
					case setClearRecord:
						break;
					
					case setRecordTransStop:
						p_Task ->	sPackageRes.CommCgy = PhoneCtrlReq_RecordTableStop;
						result = PROCESS_SUCCESS;
						break;
					
					case setReadTotalLength:
						p_Task ->	sPackageRes.CommCgy = PhoneCtrlReq_RecordTablelen;
						result = PROCESS_SUCCESS;
						break;
					
					case setReadRecord:
						p_Task ->	sPackageRes.CommCgy = PhoneCtrlReq_RecordTableData;
					
						uint16_t tempRecordIndex_Buf = ((sPhoneCtrlReq_31.RecordTableIndex_H << 8 ) | sPhoneCtrlReq_31.RecordTableIndex_L);
						NRF_LOG_DEBUG("sPhoneCtrlReq_31 -> Record Index : %d ", tempRecordIndex_Buf);
						NRF_LOG_DEBUG("sPhoneCtrlReq_31 -> Record Counter : %d ", sPhoneCtrlReq_31.RecordTableReadCnt);
						result = PROCESS_SUCCESS;
						break;
				}
			}		
			else
			{
				p_Task -> sPackageRes.CommCgy = 0;
				result = PROCESS_ERR_CTRL_ITEM;
			}
		}
			break;
		
		case cPhoneCtrl_Rsp:
		{
			uint8_t *tempPackageData_Array = pvPortMalloc(p_Task -> sPackage.Length * sizeof(uint8_t));
			
			tempPackageData_Array[0] = p_Task -> sPackage.OBDHeader;
			tempPackageData_Array[1] = p_Task -> sPackage.CategoryID;
			tempPackageData_Array[2]	= p_Task -> sPackage.Length;	
			memcpy(tempPackageData_Array + PROTOCOL_PACKAGE_HEADER_LENGTH, 
						 &p_Task-> sPackage.Data,
						 p_Task -> sPackage.Length - PROTOCOL_PACKAGE_HEADER_LENGTH);
			
#ifdef forProtocolNRF_LOG
			NRF_LOG_DEBUG("tempPackageData_Array :");
			for(uint8_t i = 0; i < p_Task -> sPackage.Length; i++)
			{
				NRF_LOG_RAW_INFO(" %02X ", tempPackageData_Array[i]);
			}
			NRF_LOG_RAW_INFO("\r\n");									
#endif				
			
			memcpy(&sPhoneCtrlRsp_B1, tempPackageData_Array, PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH);
			
			uint8_t tempExtendedLength = sPhoneCtrlRsp_B1.Length - PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH;
			
			switch(sPhoneCtrlRsp_B1.AccountAccess)
			{
				case UnusedField:
					break;
				
				case RspKeyAns:
				{
					uint8_t *tempExtendedData_Array = pvPortMalloc(tempExtendedLength * sizeof(uint8_t));
					memcpy(tempExtendedData_Array, 
								 tempPackageData_Array + PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH, 
								 tempExtendedLength);

#ifdef forProtocolNRF_LOG
					NRF_LOG_DEBUG("tempExtendedData_Array :");
					for(uint8_t i = 0; i < tempExtendedLength; i++)
					{
						NRF_LOG_RAW_INFO(" %02X ", tempExtendedData_Array[i]);
					}
					NRF_LOG_RAW_INFO("\r\n");									
#endif						
					
					/* Calculate KeyAns is correct or not */
					vPortFree(tempExtendedData_Array);
					
					/* if KeyAns is correct */
					{
						p_Task -> sPackageRes.CommCgy = PhoneCtrlRsp_KeyAnswer;
						result = PROCESS_SUCCESS;
					}
					
					/* KeyAns Error */
					{
//						*Output_Command = 0;
//						result = PROCESS_ERR_RSP_KEY_ANS;
					}
				}
					break;
			}
		}
			break;
	}
	
	return result;
}

static void Protocol_ResponsePackageTrans(uint8_t RspCategory)
{
	BaseType_t xHigherPriorityTaskWoken;
	BaseType_t err;
	uint8_t PackageExtendedLength_Buf = 0;
	
	memset(&sBLECtrlRsp_A9, 0, sizeof(sBLECtrlRsp_A9));
	
	sBLECtrlRsp_A9.OBD_Header = ohBluetooth;
	sBLECtrlRsp_A9.CategoryID = cBLECtrl_Rsp;
	
	switch(RspCategory)
	{
		case Rsp_SendSeed:
			PackageExtendedLength_Buf =  (sizeof(seed) / sizeof(seed[0]));
			sBLECtrlRsp_A9.AccountAccessState = SendSeed;
			break;
		
		case Rsp_AnsSuccess:
			sBLECtrlRsp_A9.AccountAccessState = AnsSuccess;
			break;
		
		case Rsp_AnsFailure:
			sBLECtrlRsp_A9.AccountAccessState = AnsFailure;
			break;
		
		case Rsp_DiagTransStart:
			sBLECtrlRsp_A9.DiagTransState 		= DiagTransStart;
			break;
		
		case Rsp_DiagTransSTOP:
			sBLECtrlRsp_A9.DiagTransState 		= DiagTransSTOP;
			break;
		
		case Rsp_RecordTableLen:
			sBLECtrlRsp_A9.ExtenPatloadTransState = ReturnTotalLength;
			sBLECtrlRsp_A9.PayloadCategoryState 	= 1;									/* UART Rsp */
			sBLECtrlRsp_A9.RecordTableTotalLen_H = (666 & 0xFF00) >> 8;									/* UART Rsp */
			sBLECtrlRsp_A9.RecordTableTotalLen_L = (666 & 0x00FF);
			break;
		
		case Rsp_RecordTableData:
			PackageExtendedLength_Buf = PROTOCOL_RECORDTABLE_ERRRECORD_LENGTH * sPhoneCtrlReq_31.RecordTableReadCnt;
			sBLECtrlRsp_A9.ExtenPatloadTransState = RecordTransStart;
			sBLECtrlRsp_A9.PayloadCategoryState 	= 1;
			sBLECtrlRsp_A9.RecordTableIndexRetn_L = sPhoneCtrlReq_31.RecordTableIndex_L;
			sBLECtrlRsp_A9.RecordTableIndexRetn_H = sPhoneCtrlReq_31.RecordTableIndex_H;
			sBLECtrlRsp_A9.RecordTableCntReturn = sPhoneCtrlReq_31.RecordTableReadCnt;
			break;
		
		case Rsp_RecordTableStop:
			sBLECtrlRsp_A9.ExtenPatloadTransState = RecordTransStop;
			break;
	}
	
	sBLECtrlRsp_A9.Length = PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH + PackageExtendedLength_Buf;
	
	uint8_t *tempSendRes_Array = pvPortMalloc(sBLECtrlRsp_A9.Length * sizeof(uint8_t));
	memcpy(tempSendRes_Array, &sBLECtrlRsp_A9, PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH);
	
	switch(RspCategory)
	{
		case Rsp_SendSeed:
			memcpy(tempSendRes_Array + PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH,
					 seed,
					 sizeof(seed) / sizeof(seed[0]));
			break;
		
		case Rsp_RecordTableData:
		{
			/* Load the Record Table from VCU */
			
			//test
			uint8_t *tempRecordTable_Array = pvPortMalloc(PackageExtendedLength_Buf * sizeof(uint8_t));
			for(uint8_t i = 0; i < PackageExtendedLength_Buf; i++)
			{
				tempRecordTable_Array[i] = i;
			}
		
			memcpy(tempSendRes_Array + PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH,
						 tempRecordTable_Array,
						 PackageExtendedLength_Buf);
			
			vPortFree(tempRecordTable_Array);
		} break;
		
		default:
			break;
	}
	
#ifdef forProtocolNRF_LOG
	NRF_LOG_DEBUG("tempSendRes_Array :");
	for(uint8_t i = 0; i < PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH; i++)
	{
		NRF_LOG_RAW_INFO(" %02X ", tempSendRes_Array[i]);
	}
	NRF_LOG_RAW_INFO("\r\n");									
#endif
	
	if(Q_BluetoothDataTx != NULL)
	{
		err = xQueueGenericSend(Q_BluetoothDataTx, tempSendRes_Array, xHigherPriorityTaskWoken, queueOVERWRITE);
		if(err == errQUEUE_FULL)
		{
			NRF_LOG_ERROR("Q_BluetoothDataTx is Full");
		}
		if(xHigherPriorityTaskWoken)
			portYIELD_FROM_ISR(xHigherPriorityTaskWoken); 
	}								
	vPortFree(tempSendRes_Array);
}
