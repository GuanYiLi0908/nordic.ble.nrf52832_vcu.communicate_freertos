

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "Communication_UART.h"
#include "Communication_Bluetooth.h"
#include "TPMS.h"
#include "Protocol_PackageDefine.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

struct AEON_TPMS_LinkList *NodeBegin 			 = NULL;
struct AEON_TPMS_LinkList *NodeNow  			 = NULL;
struct AEON_TPMS_LinkList *NodePrevious 	 = NULL;

extern MANUFACTURER_DATA_RECEIVE ManuFacDataRx;
extern TPMSINFORMATION sTPMSinfo_B9;
extern TaskHandle_t N_ManufacSpecDataPackagingFinish;
extern QueueHandle_t Q_TPMStoBluetooth;
extern QueueHandle_t Q_TPMStoUART;

static WhiteListImport_t m_whitelist[IMPORT_WHITELIST_ARRAY_RAW_NUMBER] = 
{
	TPMS_Device1, 
	TPMS_Device2, 
	TPMS_Device3	
};

/******************************************************************************/
/*                 	  	 				White List Task					                	  	*/
/******************************************************************************/
void TPMSTask(void *pvParameters)
{
	BeaconWhiteList_Set(m_whitelist);
//	BeaconWhiteList_Display();
	
	while(1)
	{
		BeaconWhiteList_Matching(&ManuFacDataRx);
		vTaskDelay(1);
	}
}

/******************************************************************************/
/*                 	  	 WhiteList Function subroutine	                	  	*/
/******************************************************************************/
uint32_t BeaconWhiteList_Set(WhiteListImport_t *p_ListImport)
{
	uint8_t tempDeviceID_Array[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH] = { 0 };
	
	/* Whitelist Import */
	for(uint8_t i = 0; i < IMPORT_WHITELIST_ARRAY_RAW_NUMBER; i++)
	{
		memcpy(tempDeviceID_Array, &p_ListImport[i], MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
		uint16_t tempCheck_Buf = tempDeviceID_Array[0] +
														 tempDeviceID_Array[1] +
														 tempDeviceID_Array[2] +
														 tempDeviceID_Array[3];
		if(tempCheck_Buf != 0)
		{
#ifdef forTPMSinfoB9NRF_LOG
			NRF_LOG_RAW_INFO("Import Whitelist Device ID : %02X, %02X, %02X, %02X \r\n", p_ListImport[i].DeviceAddress[0], 
																																									 p_ListImport[i].DeviceAddress[1],
																																									 p_ListImport[i].DeviceAddress[2],
																																									 p_ListImport[i].DeviceAddress[3]);
#endif
			
			/* Creat new node */
			NodeNow = (struct AEON_TPMS_LinkList*)pvPortMalloc(sizeof(struct AEON_TPMS_LinkList));
			if(NodeNow == NULL)
			{
				NRF_LOG_ERROR("Not enough memory to create");
				return NRF_ERROR_NO_MEM;
			}
			
			/* Initialization node */
			memset(NodeNow -> DeviceID, 0, MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
			NodeNow -> p_NextNode = NULL;
			
			/* Push into linklist */
			memcpy(NodeNow -> DeviceID, &p_ListImport[i], MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
			NodeNow -> DeviceNum = i;
			
			if(NodeBegin == NULL)
				NodeBegin = NodeNow;											/* 如果起始節點為空, 則將當前節點設為起始節點 */			
			else
				NodePrevious -> p_NextNode = NodeNow; 		/* 將前一個節點p_NextNode指向當前節點 */
			NodeNow -> p_NextNode = NULL;								/* 把當前節點的p_NexrNode設為NULL */
			NodePrevious = NodeNow;											/* 把前一個節點設為當前節點 */	
		}
	}
	return NRF_SUCCESS;
}

uint32_t BeaconWhiteList_Matching(MANUFACTURER_DATA_RECEIVE *p_manufacDataRx)
{
	uint8_t tempReceiveData_Array[PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH] = { 0 };
	uint8_t res = NRF_ERROR_NOT_FOUND;
	
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
	/* Store the handle of the calling task. */
	N_ManufacSpecDataPackagingFinish = xTaskGetCurrentTaskHandle();
	
	/* Get the Notification */
	if(ulTaskNotifyTake(pdFALSE, portMAX_DELAY))
	{
		NodeNow = NodeBegin;
		
		for(uint8_t i = 0; i < IMPORT_WHITELIST_ARRAY_RAW_NUMBER; i++)
		{
			if(memcmp(NodeNow -> DeviceID, &p_manufacDataRx -> DeviceID, MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH) == 0)
			{				
				switch(NodeNow -> DeviceNum)
				{
					case 0:
						/* Sensor 1 */
						memcpy(&sTPMSinfo_B9.Sensor1_DeviceID, NodeNow -> DeviceID, MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
						sTPMSinfo_B9.Sensor1_Pressure = p_manufacDataRx -> Pressure;
						sTPMSinfo_B9.Serson1_Temper = p_manufacDataRx -> Temperature;
						break;
					
					case 1:
						/* Sensor 2 */
						memcpy(&sTPMSinfo_B9.Sensor2_DeviceID, NodeNow -> DeviceID, MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
						sTPMSinfo_B9.Sensor2_Pressure = p_manufacDataRx -> Pressure;
						sTPMSinfo_B9.Serson2_Temper = p_manufacDataRx -> Temperature;
						break;
					
					case 2:
						/* Sensor 3 */
						memcpy(&sTPMSinfo_B9.Sensor3_DeviceID, NodeNow -> DeviceID, MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
						sTPMSinfo_B9.Sensor3_Pressure = p_manufacDataRx -> Pressure;
						sTPMSinfo_B9.Serson3_Temper = p_manufacDataRx -> Temperature;
						break;
					
					default:
						break;
				}
				
				memcpy(tempReceiveData_Array, &sTPMSinfo_B9, PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH);

#ifdef forTPMSinfoB9NRF_LOG
				NRF_LOG_DEBUG("sTPMSinfo_B9 :");
				for(uint8_t i = 0; i < BasicPackageLength; i++)
				{
					NRF_LOG_RAW_INFO(" %02X ", tempReceiveData_Array[i]);
				}
				NRF_LOG_RAW_INFO("\r\n");
#endif
					
				/* Import Data to the Queue */				
				if(xQueueGenericSend(Q_TPMStoUART, &tempReceiveData_Array, xHigherPriorityTaskWoken, queueOVERWRITE) != pdPASS)
					NRF_LOG_ERROR("Q_WhiteListToUART is Full \r\n");
					
				NodeNow -> PreviousTemperature = p_manufacDataRx -> Temperature;
				NodeNow -> PreviousPressure = p_manufacDataRx -> Pressure;
					
				res = NRF_SUCCESS;
				break;
			}	
			else
				NodeNow = NodeNow -> p_NextNode;
		}
	}
	else
		res = NRF_ERROR_INVALID_STATE;
	
	return res;
}

void BeaconWhiteList_Display(void)
{
	NodeNow = NodeBegin;
	while(NodeNow != NULL)
	{
		NRF_LOG_RAW_INFO("Device ID : %02X, %02X, %02X, %02X \r\n", NodeNow -> DeviceID[0], 
																																NodeNow -> DeviceID[1],
																																NodeNow -> DeviceID[2],
																																NodeNow -> DeviceID[3]);
		NodeNow = NodeNow -> p_NextNode;
	}
}

//void BeaconWhiteList_Release(void)
//{
//	Body = Begin;
//	while(Body != NULL)
//	{
//		End = Body;
//		Body = Body->next;
//		free(End);
//	}
//}
