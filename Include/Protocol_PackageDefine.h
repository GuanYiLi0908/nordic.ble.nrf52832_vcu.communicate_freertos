

#ifndef __PROTOCOL_PACKAGEDEFINE_H
#define __PROTOCOL_PACKAGEDEFINE_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "Communication_Bluetooth.h"
		
		/* Package Length */
		#define PROTOCOL_DIAGNOSTICS_PACKAGE_TOTAL_LENGTH 	27
		#define PROTOCOL_COMMAND_PACKAGE_BASIC_LENGTH				27
		#define PROTOCOL_COMMAND_PACKAGE_MAXIMUM_LENGTH			91
		#define PROTOCOL_PACKAGE_HEADER_LENGTH 							3
		
		/* OBD Header */
		#define ohVCU								0x1A		/* VCU -> BLE */
		#define ohBluetooth					0xBE		/* BLE -> VCU, CellPhoneApp */
		#define ohCellPhoneApp			0xDA		/* CellPhoneApp -> BLE */
		
		/*** Communication Command Category ID ***/
			/** VCU **/
			#define cVCUCtrl_Req			0x01
			#define cVCUCtrl_Rsp			0x81
			
			/** BLE **/
			#define cBLECtrl_Req			0x29
			#define cBLECtrl_Rsp			0xA9	
			
			/** Phone **/
			#define cPhoneCtrl_Req		0x31
			#define cPhoneCtrl_Rsp		0xB1
		
		/*** Diagnostics Information Category ID ***/
			#define PROTOCOL_DIAGNOSTICS_INFORMATION_TABLE_NUM	13
			
			/** VCU Information **/
			#define tVCUHwStatus				0x89	/* 1 */
			#define tVCUMileageAndErr		0x8A	/* 2 */
			#define tVCUBasciInfo				0x8B	/* 3 */
		
			/** Motor Control Unit(Driver) Infomation **/
			#define tDrvHwStatus				0x91	/* 1 */
			#define tDrvBasciInfo				0x92 	/* 2 */ 
			
			/** BMS Information **/
			#define tBMSSysStatus				0x99	/* 1 */
			#define tBMSGroupStatus			0x9A	/* 2 */
			#define tBMSPackageStatus_1	0x9B	/* 3 - Package1 ~ 3 */
			#define tBMSPackageStatus_2	0x9C	/* 4 - Package4 ~ 6 */
			#define tBMSPackageStatus_3	0x9D	/* 5 - Package7,8,Failure */
			
			/** ICM Information **/
			#define tICMInfo						0xA1		
			
			/** TPMS Information **/
			#define tTPMSInfo						0xB9
			#define tTPMSManufacInfo		0xBA
		
			/** Emergency **/
			#define tEmergencyErrInfo		0xD9
			
/**** UART ****/
		/*** VCU Control Req. (0x01) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;			
			
			/* Group 1 - Control */
			uint8_t OperationMode	:2;
			uint8_t 							:6;		//Reserved
			uint8_t Group1Reserved[7];
			
			/* Group 2 - Reserved */
			uint8_t Group2Reserved[8];
			
			/* Group 3 - Reserved */
			uint8_t Group3Reserved[8];			
		}VCUCONTROLREQ;		

/**** Bluetooth ****/		
		/*** Bluetooth Contrl Rsp. (0xA9) ***/
		typedef struct 
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;			
			
			/* Control Status */
			uint8_t ModeSelectState					:4;		//LSB
			uint8_t AccountAccessState 			:3;
			uint8_t 												:1;		//Reserved, MSB			
			
			uint8_t	DiagTransState					:2;		//LSB
			uint8_t 												:6;		//Reserved, MSB
			
			uint8_t ExtenPatloadTransState	:3;
			uint8_t 												:5;
			
			uint8_t PayloadCategoryState		:1;		//TableID
			uint8_t 												:7;
			
			uint8_t RecordTableTotalLen_L;
			uint8_t RecordTableTotalLen_H;
			
			uint8_t RecordTableIndexRetn_L;
			uint8_t RecordTableIndexRetn_H 	:4;
			uint8_t RecordTableCntReturn		:3;
			uint8_t													:1; 
			
			/* Group 2 - Reserved */
			uint8_t RegenModeState					:3;
			uint8_t 												:5;
			uint8_t Group2Reserved[7];
			
			/* Group 3 - Reserved */
			uint8_t Group3Reserved[8];
		}BLUETOOTHCONTROLRSP;
		
/**** Phone ****/		
		/*** Cellphone Contrl Req. (0x31) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;			
			
			/* Control */
			uint8_t ModeSelect 							:4;		//LSB	
			uint8_t AccountAccess 					:1;
			uint8_t DiagTransCtrl						:2;
			uint8_t 												:1;		//Reserved, MSB		
			
			uint8_t ExtenPatloadTransCtrl		:3;
			uint8_t 												:5;
			
			uint8_t PayloadCategory					:1;		//TableID
			uint8_t 												:7;
			
			uint8_t RecordTableIndex_L;
			uint8_t RecordTableIndex_H			:4;
			uint8_t RecordTableReadCnt			:3;
			uint8_t													:1;
			
			uint8_t Group1Reserved[3];			
			
			/* Group 2 - Reserved */
			uint8_t RegenModeCtrl						:3;
			uint8_t 												:5;
			uint8_t Group2Reserved[7];
			
			/* Group 3 - Reserved */
			uint8_t Group3Reserved[8];
		}CELLPHONECONTROLREQ;

		/*** Cellphone Contrl Rsp. (0xB1) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;			

			/* Control */
			uint8_t AccountAccess 					:2;		//LSB
			uint8_t 												:6;		//Reserved, MSB
			uint8_t Group1Reserved[7];		
			
			/* Group 2 - Reserved */
			uint8_t Group2Reserved[8];
			
			/* Group 3 - Reserved */
			uint8_t Group3Reserved[8];
		}CELLPHONECONTROLRSP;
		
/**** Diagnostics ****/		
		/*** VCU Hardware Status (0x89) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;	

			/* Group 1 - Mode Set */
			uint8_t DynamicModeComman				:4;
			uint8_t RegeonModeComman				:3;
			uint8_t 												:1;		//Reserved
			
			uint8_t 												:8;		//Reserved
			uint8_t ThrottleOpenedPosition[2];
			uint8_t Group1Reserved[4];
			
			/* Group 2 - Immediate Informaiotn */
			uint8_t DC12v[2];
			uint8_t B12v[2];
			uint8_t Speed[2];
			uint8_t ChargeState;
			uint8_t Group2Reserved;
			
			/* Group 3 - Switch Status */
			uint8_t DiagnosticState					:1;
			uint8_t CollisionProtection			:1;
			uint8_t RolloverProtection			:1;
			uint8_t 												:5;
			
			uint8_t KeySwitch								:1;
			uint8_t TouchEN									:1;
			uint8_t HandleBrakeSW_R					:1;
			uint8_t HandleBrakeSW_L					:1;
			uint8_t FootBrakeSW							:1;
			uint8_t TrunkBState							:1;
			uint8_t ParkingState						:1;
			uint8_t WiperSwitch							:1;
			
			uint8_t HeadLight								:2;
			uint8_t DRL_Front								:1;
			uint8_t DRL_Rear								:1;
			uint8_t Winker_Rear							:2;
			uint8_t Winker_Front						:2;
			
			uint8_t BrakeLight_Rear					:1;
			uint8_t HazardLight							:1;
			uint8_t 												:6;		//Reserved

			uint8_t Group3Reserved[4];
		}VCUHARDWARESTATUS;
		
		/** VCU Mileage Information (0x8A)**/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;	

			/* Group 1 - Mileage Information */		
			uint8_t TotalMileage[3];
			uint8_t TripMileage[3];
			uint8_t RangeMileage[2];
			
			/* Group 2 - VCU Faults and Error */
			uint8_t Group2Reserved[8];
			
			/* Group 3 - Reserved */ 
			uint8_t Group3Reserved[8];			
		}VCUMILEAGEINFORMATION;
		
		/*** VCU Basic Informaiotn (0x8B)***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;	

			/* Group 1 - VCU Manufacture information */ 
			uint8_t ManufacYear;
			uint8_t ManufacMonth;
			uint8_t ManufacID;
			uint8_t VCU_PN;
			uint8_t VCU_SN[4];
			
			/* Group 2 - VCU Sw/Hw Version information */
			uint8_t HwVersion[2];
			uint8_t SwVersion[2];
			uint8_t BootVersion[2];
			uint8_t Group2Reserved[2];
			
			/* Group 3 - Reserved */ 
			uint8_t Group3Reserved[8];
		}VCUBASICINFO;

		/*** Driver Hardware Status (0x91) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;	

			/* Group 1 - Mode Status */
			uint8_t DynamicModeStatus				:4;
			uint8_t RegenModeStatus					:3;
			uint8_t RegenStatus							:1;

			uint8_t DCValtage[2];
			uint8_t DCCurrent[2];
			uint8_t PhaseCurrent[2];
			
			/* Group 2 - Working Status */
			uint8_t MotorTemp;
			uint8_t DriverTemp;
			uint8_t MotorRPM[2];
			uint8_t MotorAngle[2];
			uint8_t Group2Reserved[2];
			
			/* Group 3 - Error Status */				
			uint8_t Group3Reserved[8];
		}DRIVERHARDWARESTATUS;
		
		/*** Driver Basic Informaiotn (0x92)***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;	

			/* Group 1 - Driver Manufacture information */ 
			uint8_t ManufacYear;
			uint8_t ManufacMonth;
			uint8_t ManufacID;
			uint8_t Drv_PN;
			uint8_t Drv_SN[4];
			
			/* Group 2 - Driver Sw/Hw Version information */
			uint8_t HwVersion[2];
			uint8_t SwVersion[2];
			uint8_t BootVersion[2];
			uint8_t Group2Reserved[2];
			
			/* Group 3 - Reserved */ 
			uint8_t Group3Reserved[8];
		}DRIVERBASICINFO;		
		
		/*** BMS System Diagnostics Information (0x99) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;

			/* Group 1 - Battery System Status */
			uint8_t SystemCapacity[2];
			uint8_t SystemVoltage[2];
			uint8_t SystemCurrent[2];
			uint8_t SysAvailableCap[2];
			
			/* Group 2 - Battery System Limitation */
			uint8_t SysDischargeCurntLimit[2];
			uint8_t SysChargeCurntLimit[2];
			uint8_t Group2Reserved[4];
			
			/* Group 3 - Reserved */ 
			uint8_t Group3Reserved[8];
		}BMSSYSTEMINFORMATION;
		
		/*** BMS Group Diagnostics Information (0x9A) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;

			/* Group 1 - Battery Group 1 Status */
			uint8_t Group1_Capacity[2];
			uint8_t Group1_Voltage[2];
			uint8_t Group1_Current[2];
			uint8_t Group1_AvailableCap[2];
			
			/* Group 2 - Battery Group 2 Status */
			uint8_t Group2_Capacity[2];
			uint8_t Group2_Voltage[2];
			uint8_t Group2_Current[2];
			uint8_t Group2_AvailableCap[2];
			
			/* Group 3 - Battery Group Limitation */
			uint8_t Group1_DischargeCurntLimit[2];
			uint8_t Group1_ChargeCurntLimit[2];
			uint8_t Group2_DischargeCurntLimit[2];
			uint8_t Group2_ChargeCurntLimit[2];
		}BMSGROUPINFORMATION;
	
		/*** BMS Package Diagnostics Information 1 (0x9B) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;

			/* Group 1 - Battery Package 1 Status */
			uint8_t Pack1_SOC;
			uint8_t Pack1_Capacity[2];
			uint8_t Pack1_Voltage[2];
			uint8_t Pack1_Current[2];
			uint8_t Group1Reserved;
			
			/* Group 2 - Battery Package 1 Limitation */
			uint8_t Pack1_MaxCellTemp;
			uint8_t Pack1_MinCellTemp;
			uint8_t Pack1_MaxMOSTemp;
			
			uint8_t Pack1_ChargeMOS					:1;
			uint8_t Pack1_DischargeMOS			:1;
			uint8_t 												:6;

			uint8_t Pack1_MaxCellVol[2];
			uint8_t Pack1_MinCellVol[2];
			
			/* Group 3 - Battery Package 2 Status */
			uint8_t Pack2_SOC;
			uint8_t Pack2_Capacity[2];
			uint8_t Pack2_Voltage[2];
			uint8_t Pack2_Current[2];
			uint8_t Group3Reserved;
		}BMSPACKAGEINFORMATION1;

		/*** BMS Package Diagnostics Information 2 (0x9C) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;

			/* Group 1 - Battery Package 2 Limitation */
			uint8_t Pack2_MaxCellTemp;
			uint8_t Pack2_MinCellTemp;
			uint8_t Pack2_MaxMOSTemp;
			
			uint8_t Pack2_ChargeMOS					:1;
			uint8_t Pack2_DischargeMOS			:1;
			uint8_t 												:6;
			
			uint8_t Pack2_MaxCellVol[2];
			uint8_t Pack2_MinCellVol[2];
			
			/* Group 2 - Battery Package 3 Status */
			uint8_t Pack3_SOC;
			uint8_t Pack3_Capacity[2];
			uint8_t Pack3_Voltage[2];
			uint8_t Pack3_Current[2];
			uint8_t Group2Reserved;
			
			/* Group 3 - Battery Package 2 Status */
			uint8_t Pack3_MaxCellTemp;
			uint8_t Pack3_MinCellTemp;
			uint8_t Pack3_MaxMOSTemp;
			
			uint8_t Pack3_ChargeMOS					:1;
			uint8_t Pack3_DischargeMOS			:1;
			uint8_t 												:6;
			
			uint8_t Pack3_MaxCellVol[2];
			uint8_t Pack3_MinCellVol[2];
		}BMSPACKAGEINFORMATION2;

		/*** BMS Package Diagnostics Information 3 (0x9D) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;

			/* Group 1 - Battery Package 4 Status */
			uint8_t Pack4_SOC;
			uint8_t Pack4_Capacity[2];
			uint8_t Pack4_Voltage[2];
			uint8_t Pack4_Current[2];
			uint8_t Group1Reserved;
			
			/* Group 2 - Battery Package 3 Limitation */
			uint8_t Pack4_MaxCellTemp;
			uint8_t Pack4_MinCellTemp;
			uint8_t Pack4_MaxMOSTemp;
			
			uint8_t Pack4_ChargeMOS					:1;
			uint8_t Pack4_DischargeMOS			:1;
			uint8_t 												:6;

			uint8_t Pack4_MaxCellVol[2];
			uint8_t Pack4_MinCellVol[2];
			
			/* Group 3 - Battery Failure Information */			
			uint8_t Group3Reserved[8];
		}BMSPACKAGEINFORMATION3;
		
		/*** ICM Diagnostics Information (0xA1) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;
		}ICMINFORMATION;			
		
		/*** TPMS Diagnostics Information (0xB9) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;
			
			/* Group 1 - TPMS Sensor 1 Status */
			uint8_t Sensor1_DeviceID[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
			uint8_t Sensor1_Pressure;
			uint8_t Serson1_Temper;	
			uint8_t Serson1_StatusReserved[2];
			
			/* Group 2 - TPMS Sensor 2 Status */
			uint8_t Sensor2_DeviceID[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
			uint8_t Sensor2_Pressure;
			uint8_t Serson2_Temper;	
			uint8_t Serson2_StatusReserved[2];
			
			/* Group 3 - TPMS Sensor 3 Status */
			uint8_t Sensor3_DeviceID[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
			uint8_t Sensor3_Pressure;
			uint8_t Serson3_Temper;	
			uint8_t Serson3_StatusReserved[2];			
		}TPMSINFORMATION;

		/*** TPMS Manufacture Information (0xBA) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;

			/* Group 1 - TManufacturing Information */
			uint8_t ManufacYear;
			uint8_t ManufacMonth;
			uint8_t ManufacID;
			uint8_t TPMS_PN;
			uint8_t TPMS_SN[4];
			
			/* Group 3 - Reserved */ 
			uint8_t HwVersion[2];
			uint8_t SwVersion[2];
			uint8_t BootVersion[2];
			uint8_t Group2Reserved[2];
			
			/* Group 3 - Reserved */ 
			uint8_t Group3Reserved[8];			
		}TPMSMANUFACINFORMATION;
			
		/*** Emergency Error Information (0xD9) ***/
		typedef struct
		{
			/* Header */
			uint8_t OBD_Header;
			uint8_t CategoryID;
			uint8_t Length;

			/* Group 1 - Error Flag */
			
			//#0
			uint8_t Battery12VoltageErr			:1;
			uint8_t LCD12vErr								:1;
			uint8_t BatteryPack12vErr				:1;
			uint8_t BatterySecurityErr			:1;
			uint8_t Battery12vChargerErr		:1;
			uint8_t Horn12vErr							:1;
			uint8_t Fuse12vErr							:1;
			uint8_t MCU12vErr								:1;
			
			//#1
			uint8_t DC1VoltageErr						:1;
			uint8_t Throttle5vErr						:1;
			uint8_t TP12vErr								:1;
			uint8_t BatterySysSOClow				:1;
			uint8_t DC2VoltageErr						:1;
			uint8_t TPSVoltageLow						:1;
			uint8_t TPSVoltageHigh					:1;
			uint8_t TPSErr									:1;			

			//#2
			uint8_t MCUCAN0Offline					:1;
			uint8_t DashboardCAN1Offline		:1;
			uint8_t BatterySysCAN0Offline		:1;
			uint8_t BatterySysCurrentErr		:1;
			uint8_t TurnkOpen								:1;
			uint8_t TPMSOffline							:1;
			uint8_t Head12vErr							:1;
			uint8_t 												:1;	

			//#3
			uint8_t PackErr_P3001						:1;
			uint8_t PackErr_P3002						:1;
			uint8_t PackErr_P3003						:1;
			uint8_t PackErr_P3004						:1;
			uint8_t PackErr_P3005						:1;
			uint8_t PackErr_P3006						:1;
			uint8_t PackErr_P3007						:1;
			uint8_t PackErr_P3008						:1;
			
			//#4
			uint8_t PackErr_P3009						:1;
			uint8_t PackErr_P3010						:1;
			uint8_t PackErr_P3011						:1;
			uint8_t PackErr_P3012						:1;
			uint8_t PackErr_P3013						:1;
			uint8_t PackErr_P3014						:1;
			uint8_t PackErr_P3015						:1;
			uint8_t 												:1;
			
			//#5
			uint8_t SysCANCommuniFault			:1;
			uint8_t BMSNFCCommuniFault			:1;
			uint8_t 												:6;
			
			//#6
			uint8_t ABIEncoderErr						:1;
			uint8_t PWMEncoderErr						:1;
			uint8_t DriverOVP								:1;
			uint8_t DriverUVP								:1;
			uint8_t iPHHwOCP								:1;
			uint8_t iDCHwOCP								:1;
			uint8_t MCUTPSErr								:1;
			uint8_t PreChargeErr						:1;
		
			//#7
			uint8_t iPHSwOCP								:1;
			uint8_t iDCSwOCP								:1;
			uint8_t MotorOTP								:1;
			uint8_t DriverOTP								:1;
			uint8_t BlockProtect						:1;
			uint8_t VCUCommuniFault					:1;
			uint8_t EEPROMErr								:1;
			uint8_t 												:1;

			/* Group 3 - Reserved */ 
			uint8_t MotorHighTempAlarm			:1;
			uint8_t DriverHighTempAlarm			:1;
			uint8_t 												:4;
			uint8_t TPMSTempErr							:1;
			uint8_t TPMSBatteryLow					:1;
			uint8_t 												:1;
			uint8_t Group2Reserved[7];
			
			/* Group 3 - Reserved */ 
			uint8_t Group3Reserved[8];			
		}EMERGENCYERRORINFO;
		
/**** Record Table ****/
		/*** Historic Error Data (Extened) **/
		typedef struct
		{
			
			/* Group 1 - */
			uint8_t ErrRecordAmount_LSB;
			uint8_t ErrRecordAmount_MSB			:4;
			
			uint8_t ErrRecordNumber_LSB			:4;
			uint8_t ErrRecordNumber_MSB;
			
			uint8_t TimeStamp[4];
			
			uint8_t Battery12VoltageErr			:1;
			uint8_t LCD12vErr								:1;
			uint8_t BatteryPack12vErr				:1;
			uint8_t BatterySecurityErr			:1;
			uint8_t Battery12vChargerErr		:1;
			uint8_t Horn12vErr							:1;
			uint8_t Fuse12vErr							:1;
			uint8_t MCU12vErr								:1;
			
			/* Group 2 - */
			uint8_t DC1VoltageErr						:1;
			uint8_t Throttle5vErr						:1;
			uint8_t TP12vErr								:1;
			uint8_t BatterySysSOClow				:1;
			uint8_t DC2VoltageErr						:1;
			uint8_t TPSVoltageLow						:1;
			uint8_t TPSVoltageHigh					:1;
			uint8_t TPSErr									:1;	
			
			uint8_t MCUCAN0Offline					:1;
			uint8_t DashboardCAN1Offline		:1;
			uint8_t BatterySysCAN0Offline		:1;
			uint8_t BatterySysCurrentErr		:1;
			uint8_t TurnkOpen								:1;
			uint8_t TPMSOffline							:1;
			uint8_t Head12vErr							:1;
			uint8_t 												:1;	
			
			uint8_t PackErr_P3001						:1;
			uint8_t PackErr_P3002						:1;
			uint8_t PackErr_P3003						:1;
			uint8_t PackErr_P3004						:1;
			uint8_t PackErr_P3005						:1;
			uint8_t PackErr_P3006						:1;
			uint8_t PackErr_P3007						:1;
			uint8_t PackErr_P3008						:1;
			
			uint8_t PackErr_P3009						:1;
			uint8_t PackErr_P3010						:1;
			uint8_t PackErr_P3011						:1;
			uint8_t PackErr_P3012						:1;
			uint8_t PackErr_P3013						:1;
			uint8_t PackErr_P3014						:1;
			uint8_t PackErr_P3015						:1;
			uint8_t 												:1;

			uint8_t SysCANCommuniFault			:1;
			uint8_t BMSNFCCommuniFault			:1;
			uint8_t 												:6;
			
			uint8_t ABIEncoderErr						:1;
			uint8_t PWMEncoderErr						:1;
			uint8_t DriverOVP								:1;
			uint8_t DriverUVP								:1;
			uint8_t iPHHwOCP								:1;
			uint8_t iDCHwOCP								:1;
			uint8_t MCUTPSErr								:1;
			uint8_t PreChargeErr						:1;
		
			uint8_t iPHSwOCP								:1;
			uint8_t iDCSwOCP								:1;
			uint8_t MotorOTP								:1;
			uint8_t DriverOTP								:1;
			uint8_t BlockProtect						:1;
			uint8_t VCUCommuniFault					:1;
			uint8_t EEPROMErr								:1;
			uint8_t 												:1;

			uint8_t MotorHighTempAlarm			:1;
			uint8_t DriverHighTempAlarm			:1;
			uint8_t 												:4;
			uint8_t TPMSTempErr							:1;
			uint8_t TPMSBatteryLow					:1;
			uint8_t 												:1;
			
		}HISTORICERRTABLE;
		
#ifdef __cplusplus
}
#endif
#endif /*__PROTOCOL_PACKAGEDEFINE_H */
