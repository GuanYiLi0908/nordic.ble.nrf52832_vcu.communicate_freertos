

#ifndef __PROCESS_ERRORDEFINE_H
#define __PROCESS_ERRORDEFINE_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		
		#include <stdint.h>

		#define PROCESS_GENERAL_BASE_NUM			(0x00)
		#define PROCESS_UNWORKING													(PROCESS_GENERAL_BASE_NUM + 0)
		#define PROCESS_SUCCESS														(PROCESS_GENERAL_BASE_NUM + 1)
		
//		#define PROCESS_FINISH														(PROCESS_GENERAL_BASE_NUM + 1)
//		#define PROCESS_RESTART														(PROCESS_GENERAL_BASE_NUM + 2)
		
		#define PROCESS_ERR_BASE_NUM					(0xE0)
		#define PROCESS_ERR_UNKNOW_COMMAND									(PROCESS_ERR_BASE_NUM + 1)
		#define PROCESS_ERR_LENGTH_OUTRANGE									(PROCESS_ERR_BASE_NUM + 2)
		#define PROCESS_ERR_CTRL_ITEM												(PROCESS_ERR_BASE_NUM + 3)
		#define PROCESS_ERR_RSP_KEY_ANS											(PROCESS_ERR_BASE_NUM + 4)
		#define PROCESS_ERR_WRONG_COMMAND										(PROCESS_ERR_BASE_NUM + 5)
//		#define PROCESS_ERR_TIMEOUT												(PROCESS_ERR_BASE_NUM + 6)
//		#define PROCESS_ERR_MEM_FULL											(PROCESS_ERR_BASE_NUM + 7)



#ifdef __cplusplus
}
#endif
#endif /*__PROCESS_ERRORDEFINE_H */
