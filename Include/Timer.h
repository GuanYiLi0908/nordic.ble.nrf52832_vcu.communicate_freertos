

#ifndef __TIMER_H
#define __TIMER_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "FreeRTOS.h"
		#include "task.h"
		#include "queue.h"
		#include "semphr.h"
		#include "timers.h"
		
		#define TIMEINTERVAL_SYSTEM_ALIVE					250
		#define TIMEINTERVAL_BASIC_DIAGNOSTICS_TX	10
		#define TIMEINTERVAL_CONNECT_TIMEOUT			10000
		
		void xTimer_Initialzation(void);
		void xTimer_Start(void);
		
		static void Timer_SystemAlive_Handle(TimerHandle_t xTimer);
		static void Timer_DiagnosticsTx_Handle(TimerHandle_t xTimer);
		static void Timer_ConnectTimeout_Handle(TimerHandle_t xTimer);
		
#ifdef __cplusplus
}
#endif
#endif /*__TIMER_H */
