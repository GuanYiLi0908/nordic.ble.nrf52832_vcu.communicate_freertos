

#ifndef __COMMUNICATION_UART_H
#define __COMMUNICATION_UART_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "app_uart.h"
		
		#include "Communication_Bluetooth.h"
		#include "Protocol_PackageDefine.h"
		#include "Process_ErrorDefine.h"
		
		#define forUARTRxNRF_LOG										1
		
		/* UART */
		#define UART_TX_BUF_SIZE          					256                                   /**< UART TX buffer size. */
		#define UART_RX_BUF_SIZE          					256                                   /**< UART RX buffer size. */
		#define DATA_CRC32_LENGTH										4																				

		/*** Package Header ***/
		typedef struct
		{
			uint8_t OBDHeader;
			uint8_t CategoryID;
			uint8_t Length;
			uint8_t Data[PROTOCOL_COMMAND_PACKAGE_MAXIMUM_LENGTH - PROTOCOL_PACKAGE_HEADER_LENGTH];
		}UARTPACKAGEFORMAT;		
		

		/*** Protocol Task State Machine ***/
		typedef enum
		{
			Pkg_Header = 0,
			Pkg_CategoryID,
			Pkg_Length,
			Pkg_Data,
		}UARTHEADERPROCESSSTATUS;

		typedef struct
		{
			UARTHEADERPROCESSSTATUS eHeaderProcess;
			UARTPACKAGEFORMAT				sPackage;
		}UARTTASK;

		void UARTTask(void *pvParameters);
		static uint8_t UART_PackageHeaderDetermine(UARTTASK *p_Task);
		
		void UART_Initialization(void);
		static void UARTEvent_handler(app_uart_evt_t * p_event);

#ifdef __cplusplus
}
#endif
#endif /*__COMMUNICATION_UART_H */
