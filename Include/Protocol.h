

#ifndef __PROTOCOL_H
#define __PROTOCOL_H
#ifdef __cplusplus
 extern "C" {
#endif

		#include <stdint.h>
		#include "Protocol_PackageDefine.h"
		
//		#define forProtocolNRF_LOG 1
		
			#define UnusedField					0
		/*** Cellphone Contrl Req(0x31) ***/
			/** ModeSelect **/		
			#define setStandbyMode			1
			#define setEnginerMode			2
			#define setLv1Mode					3
			#define setLv2Mode					4
			#define setLv3Mode					5
			
			/** AccountAccess **/
			#define SeedReq							1
			
			/** DiagTransCrtl **/
			#define setDiagTransSTOP		1
			#define setDiagTransStart		2
			
			/** RecordTableTransCtrl **/
			#define setClearRecord			1
			#define setRecordTransStop	2
			#define setReadTotalLength	3
			#define setReadRecord				4
			
		/*** Cellphone Contrl Rsp(0xB1) ***/
			/** ModeSelect **/
			
			/** AccountAccess **/
			#define RspKeyAns						1
			#define False								2
			
			/** TransControl **/

		/*** Bluetooth Contrl Rsp(0xA9) / Rsp ***/
			/** ModeSelectState **/
			#define Failure							1
			#define StandbyMode					2
			#define EnginerMode					3
			#define Lv1Mode							4
			#define Lv2Mode							5
			#define Lv3Mode							6
			
			/** AccountAccessState **/
			#define Rejected						1
			#define SendSeed						2
			#define AnsSuccess					3
			#define AnsFailure					4
			
			/** DiagTransState **/
			#define DiagTransSTOP				1
			#define DiagTransStart			2			
			
			/** RecordTableOptionState **/
			#define ClearingRecord		 	1
			#define RecordTransStop 		2
			#define ReturnTotalLength		3
			#define RecordTransStart		4
			
			
		/*** BluetoothCtrlRsp(0xA9) Transmit Options ***/	
		#define Rsp_SendSeed				0x01
		#define Rsp_AnsSuccess			0x02
		#define Rsp_AnsFailure			0x03
		#define Rsp_Rejected				0x04
		#define Rsp_DiagTransStart	0x05
		#define Rsp_DiagTransSTOP		0x06
		#define Rsp_RecordTableLen	0x07
		#define Rsp_RecordTableData	0x08
		#define Rsp_RecordTableStop 0x09
			
		/*** Protocol Command Analyze Result ***/
		#define PhoneCtrlReq_Seed								0x01
		#define PhoneCtrlReq_setDiagTransSTOP		0x02
		#define PhoneCtrlReq_setDiagTransStart	0x03
		#define PhoneCtrlReq_RecordTablelen			0x04
		#define PhoneCtrlReq_RecordTableData		0x05
		#define PhoneCtrlReq_RecordTableStop		0x06
		
		#define PhoneCtrlRsp_KeyAnswer					0x80			
		
			
		/*** Communication Certification Information ***/
		#define PROTOCOL_CERTIFICATION_SEED_LENGTH 32
		#define PROTOCOL_CERTIFICATION_KEY_LENGTH 32
		typedef struct
		{
			uint8_t Seed[PROTOCOL_CERTIFICATION_SEED_LENGTH];			/* Random generate */
		}COMMUNICERTIFICINFO;
		
		/*** Communication Record Table Information ***/
		#define PROTOCOL_RECORDTABLE_ERRRECORD_LENGTH 16
		
		/*** Package Header ***/
		typedef struct
		{
			uint8_t OBDHeader;
			uint8_t CategoryID;
			uint8_t Length;
			uint8_t Data[PROTOCOL_COMMAND_PACKAGE_MAXIMUM_LENGTH - PROTOCOL_PACKAGE_HEADER_LENGTH];
		}PROTOCOLPACKAGEFORMAT;

		typedef struct
		{
			uint8_t Header;
			uint8_t Category;
			uint8_t Composite;
			uint8_t CommCgy;										//Command Category
		}PROTOCOLPACKAGEDETERMINRESULT;		

		/*** Protocol Task State Machine ***/
		typedef enum
		{
			Certifi_SeedReq = 0,
			Certifi_KeyAnsRsp,
		}PROTOCOLCERTIFIPROCESSSTATUS;
		
		typedef enum
		{
			Package_Header = 0,
			Package_CategoryID,
			Package_Length,
			Package_Data,
		}PROTOCOLHEADERPROCESSSTATUS;
		
		typedef enum
		{
			ProtclTask_Certification = 0,
			ProtclTask_Idle,
			ProtclTask_Diagnostic,
			ProtclTask_RecordTableOperation,
		}PROTOCOLMAINPROCESSSTATUS;
		
		typedef struct
		{
			PROTOCOLMAINPROCESSSTATUS 		eMainProcess;
			PROTOCOLHEADERPROCESSSTATUS		eHeaderProcess;
			PROTOCOLCERTIFIPROCESSSTATUS	eCertifiProcess;
			PROTOCOLPACKAGEFORMAT 				sPackage;
			PROTOCOLPACKAGEDETERMINRESULT	sPackageRes;
			uint8_t PackageLengthCounter;
		}PROTOCOLTASK;

		void ProtocolTask(void *pvParameters);
		static uint8_t Protocol_PackageHeaderDetermine(PROTOCOLTASK *p_Task);
		static uint8_t Protocol_PackageCategoryDetermine(PROTOCOLTASK *p_Task);
		static void Protocol_ResponsePackageTrans(uint8_t RspCategory);
		
#ifdef __cplusplus
}
#endif
#endif /*__PROTOCOL_H */
