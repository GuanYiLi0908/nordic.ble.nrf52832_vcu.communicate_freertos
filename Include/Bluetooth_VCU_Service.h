

#ifndef __BLUETOOTH_VCU_SERVICE_H
#define __BLUETOOTH_VCU_SERVICE_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "sdk_config.h"
		#include "sdk_common.h"
		#include "ble.h"
		#include "ble_srv_common.h"
		#include "nrf_sdh_ble.h"
		#include "ble_link_ctx_manager.h"
		
		
		#define VCU_SERVICE_UUID 									0x3260
		#define VCU_VALUE_CHAR_UUID 							0x3261
		#define BLE_UUID_VCUS_RX_CHARACTERISTIC		0x326A
		#define BLE_UUID_VCUS_TX_CHARACTERISTIC		0x326B	
		
		#define BLE_VCUs_DEF(_name)	\
		static ble_vcus_t _name;		\
    NRF_SDH_BLE_OBSERVER(_name ## _obs,                           \
                         BLE_NUS_BLE_OBSERVER_PRIO,               \
                         ble_vcus_on_ble_evt,                      \
                         &_name)

		#define OPCODE_LENGTH        1
		#define HANDLE_LENGTH        2

		/**@brief   Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
		#if defined(NRF_SDH_BLE_GATT_MAX_MTU_SIZE) && (NRF_SDH_BLE_GATT_MAX_MTU_SIZE != 0)
				#define BLE_VCUS_MAX_DATA_LEN (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - OPCODE_LENGTH - HANDLE_LENGTH)
		#else
				#define BLE_NUS_MAX_DATA_LEN (BLE_GATT_MTU_SIZE_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH)
				#warning NRF_SDH_BLE_GATT_MAX_MTU_SIZE is not defined.
		#endif


		/**@brief   ADATA VCU Service event types. */
		typedef enum
		{
				BLE_VCUS_EVT_RX_DATA,      /**< Data received. */
				BLE_VCUS_EVT_TX_RDY,       /**< Service is ready to accept new data to be transmitted. */
				BLE_VCUS_EVT_COMM_STARTED, /**< Notification has been enabled. */
				BLE_VCUS_EVT_COMM_STOPPED, /**< Notification has been disabled. */
		} ble_vcus_evt_type_t;


		/* Forward declaration of the ble_vcus_t type. */
		typedef struct ble_vcus_s ble_vcus_t;		
		
		
		/**@brief   ADATA VCU Service @ref BLE_VCUS_EVT_RX_DATA event data.
		 *
		 * @details This structure is passed to an event when @ref BLE_VCUS_EVT_RX_DATA occurs.
		 */
		typedef struct
		{
				uint8_t const * p_data; /**< A pointer to the buffer with received data. */
				uint16_t        length; /**< Length of received data. */
		} ble_vcus_evt_rx_data_t;

		
		/**@brief ADATA VCU Service client context structure.
		 *
		 * @details This structure contains state context related to hosts.
		 */
		typedef struct
		{
				bool is_notification_enabled; /**< Variable to indicate if the peer has enabled notification of the RX characteristic.*/
		}ble_vcus_client_context_t;


		/**@brief   ADATA VCU Service event structure.
		 *
		 * @details This structure is passed to an event coming from service.
		 */
		typedef struct
		{
				ble_vcus_evt_type_t         	type;        /**< Event type. */
				ble_vcus_t                	*	p_vcus;      /**< A pointer to the instance. */
				uint16_t                   		conn_handle; /**< Connection handle. */
				ble_vcus_client_context_t 	* p_link_ctx;  /**< A pointer to the link context. */
				union
				{
						ble_vcus_evt_rx_data_t rx_data; /**< @ref BLE_NUS_EVT_RX_DATA event data. */
				} params;
		}ble_vcus_evt_t;
	
		
		/**@brief ADATA VCU Servicee event handler type. */
		typedef void (* ble_vcus_evt_handler_t) (ble_vcus_evt_t * p_evt);

		
		/**@brief Custom Service init structure. This contains all options and data needed for
		 *        initialization of the service.*/
		typedef struct
		{
//				uint8_t                       initial_custom_value;           /**< Initial custom value */
//				ble_srv_cccd_security_mode_t  custom_value_char_attr_md;      /**< Initial security level for Custom characteristics attribute */
				ble_vcus_evt_handler_t        evt_handler;                    /**< Event handler to be called for handling events in the Custom Service. */
		} ble_vcus_init_t;
		
		/**@brief Custom Service structure. This contains various status information for the service. */
		struct ble_vcus_s
		{
				uint8_t                       uuid_type; 
				uint16_t                      service_handle;                 /**< Handle of Custom Service (as provided by the BLE stack). */
				ble_gatts_char_handles_t      VCUs_Tx_handles;           			/**< Handles related to the Custom Value characteristic. */
				ble_gatts_char_handles_t      VCUs_Rx_handles;
				blcm_link_ctx_storage_t * const p_link_ctx_storage; 					/**< Pointer to link context storage with handles of all current connections and its context. */
				ble_vcus_evt_handler_t        evt_handler;                    /**< Event handler to be called for handling events in the Custom Service. */
		};

		
		/**@brief Function for initializing the Custom Service.
		 *
		 * @param[out]  p_cus       Custom Service structure. This structure will have to be supplied by
		 *                          the application. It will be initialized by this function, and will later
		 *                          be used to identify this particular service instance.
		 * @param[in]   p_cus_init  Information needed to initialize the service.
		 *
		 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
		 */
		 uint32_t Bluetooth_VCUs_Initialization(ble_vcus_t * p_cus, const ble_vcus_init_t * p_cus_init);

		/**@brief Function for handling the Application's BLE Stack events.
		 *
		 * @details Handles all events from the BLE stack of interest to the Battery Service.
		 *
		 * @note 
		 *
		 * @param[in]   p_ble_evt  Event received from the BLE stack.
		 * @param[in]   p_context  Custom Service structure.
		 */
		 void ble_vcus_on_ble_evt( ble_evt_t const * p_ble_evt, void * p_context);
		 
		 static void on_connect(ble_vcus_t * p_vcus, ble_evt_t const * p_ble_evt);
		 static void on_disconnect(ble_vcus_t * p_vcus, ble_evt_t const * p_ble_evt);
		 static void on_write(ble_vcus_t * p_vcus, ble_evt_t const * p_ble_evt);
		 static void on_hvx_tx_complete(ble_vcus_t * p_vcus, ble_evt_t const * p_ble_evt);
		 
		/**@brief   Function for sending a data to the peer.
		 *
		 * @details This function sends the input string as an RX characteristic notification to the
		 *          peer.
		 *
		 * @param[in]     p_nus       Pointer to the Nordic UART Service structure.
		 * @param[in]     p_data      String to be sent.
		 * @param[in,out] p_length    Pointer Length of the string. Amount of sent bytes.
		 * @param[in]     conn_handle Connection Handle of the destination client.
		 *
		 * @retval NRF_SUCCESS If the string was sent successfully. Otherwise, an error code is returned.
		 */
		uint32_t ble_vcus_data_send(ble_vcus_t * p_nus,
																uint8_t    * p_data,
																uint16_t   * p_length,
																uint16_t     conn_handle);
#ifdef __cplusplus
}
#endif
#endif /*__BLUETOOTH_VCU_SERVICE_H */
