

#ifndef __COMMUNICATION_BLUETOOTH_H
#define __COMMUNICATION_BLUETOOTH_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "nrf_sdh_ble.h"
		#include "nrf_ble_scan.h"
		#include "nrf_ble_gatt.h"
		#include "ble_nus.h"
		#include "ble_advertising.h"
		#include "app_timer.h"
		
//		#define forScanDataNRF_LOG 				1
//		#define forBluetoothRxNRF_LOG			1
		#define forBluetoothTxNRF_LOG			1
//		#define forASCIIResultNRF_LOG			1

		/* GAP */
		void GAP_Initialization(void);

		/* GATT */
		void GATT_Initialization(void);		
		
		/* Bluetooth */
		/**< Tag that refers to the BLE stack configuration that is set with @ref sd_ble_cfg_set. 
				 The default tag is @ref APP_BLE_CONN_CFG_TAG. >**/
		#define APP_BLE_CONN_CFG_TAG     												1                         
		
		/**< BLE observer priority of the application. There is no need to modify this value. >**/
		#define APP_BLE_OBSERVER_PRIO     											3                                     
	
		/**< Time from initiating event (connect or start of notification) 
				 to the first time sd_ble_gap_conn_param_update is called (5 seconds). >**/
		#define FIRST_CONN_PARAMS_UPDATE_DELAY  								APP_TIMER_TICKS(5000)    

		/**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). >**/
		#define NEXT_CONN_PARAMS_UPDATE_DELAY   								APP_TIMER_TICKS(30000)   

		/**< Number of attempts before giving up the connection parameter negotiation. >**/
		#define MAX_CONN_PARAMS_UPDATE_COUNT    								3                                           

		/**< The advertising interval (in units of 0.625 ms). This value corresponds to 187.5 ms. >**/
		#define APP_ADV_INTERVAL                								300                                         
		
		/**< The advertising duration (180 seconds) in units of 10 milliseconds. >**/
		#define APP_ADV_DURATION                								18000                                       

		#define ManufacSpecPayload_NoUseOffset 									7								/* Unused Information length of Manufacturer Specific Payload */
		#define MANUFACTURER_SPECIFICATION_MAJORMINOR_LENGTH 		4								/* Major and Minor data length of Bluetooth definition */
		#define MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH 				4								/* AEON TPMS Device ID length */
		#define MANUFACTURER_DEFINITION_DATA_LENGTH							2								/* TPMS Data length */
		
		#define BLEConnectStatus_Connect												0x55
		#define BLEConnectStatus_Disconnect											0xAA
		#define BLEConnectStatus_Invalid												0x0F
		
		/* BLEFunctionMode */
		#define FunctionMode_Idle																0x01
		#define FunctionMode_Diagnostic													0x10
		#define FunctionMode_RecordTableOperation								0x11
		#define FunctionMode_Regen															0x12
		
		typedef struct 
		{
			uint8_t DeviceID[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
			uint8_t Temperature;
			uint8_t Pressure;
		}MANUFACTURER_DATA_RECEIVE;
		
		typedef enum
		{
			StartConnect = 0,
			WaitCertification,
			ReceivePackage,
			TransmitPackage,
			ErrorProcess,
		}BLUETOOTHTRANSMITPROCESSSTATUS;
		
		typedef enum
		{
			BleTask_Certification = 0,
			BleTask_Idle,
			BleTask_Diagnostic,
			BleTask_RecordTable,
		}BLUETOOTHPROCESSSTATUS;		
		
		#define BLUETOOTH_TRANSMIT_DATA_LENGTH	20
		typedef struct
		{
			uint8_t TxLength;
			uint8_t TransCycle;
			uint8_t TransPosition;
		}BLUETOOTHTRANSMITINFORMAIOTN;
		
		typedef struct
		{
			BLUETOOTHTRANSMITPROCESSSTATUS		eTransProcess;
			BLUETOOTHPROCESSSTATUS 						eBLEProcess;
			BLUETOOTHTRANSMITINFORMAIOTN			sTransInfo;
		}BLUETOOTHTASK;
			
		void BluetoothTask(void *pvParameters);
		static uint8_t Bluetooth_HEX2ASCII(uint8_t *InputArray, uint8_t InputArrayLength, uint8_t *OutputArray);
		static void Bluetooth_TransmitInfoCalcu(BLUETOOTHTASK *p_Task, uint8_t PackageLength);
		void Bluetooth_DisconnectConnection(void);
		uint32_t Fble_nus_data_send(uint8_t * p_data, uint16_t * p_length);
		
		void BLE_ProtocolStack_Initialization(void);
		void Connect_Initialization(void);
		void Advertising_Initialization(void);
		void BLE_Scan_Initialization(void);
		void BLE_AdvertisingAndScanning_Start(void * p_erase_bonds);
		
		static void ScanEvent_handler(scan_evt_t const * p_scan_evt);
		static void BLEEvent_handler(ble_evt_t const * p_ble_evt, void * p_context);
		static void conn_params_error_handler(uint32_t nrf_error);
		static void on_adv_evt(ble_adv_evt_t ble_adv_evt);
		
		/* NUSc */
		#define ECHOBACK_BLE_UART_DATA  1                                       /**< Echo the UART data that is received over the Nordic UART Service (NUS) back to the sender. */

		/* Manufacturer Advertising Data */		
		#define MANUFACTURER_ADVINFO_LENGTH					0x0A
		#define MANUFACTURER_COMPANY_IDENTIFIER  		0x08D4                      /**< Company identifier for ADATA Technology Co., LTD */


		void BluetoothServices_Initialization(void);
		static void nrf_qwr_error_handler(uint32_t nrf_error);
		static void NUS_Data_handler(ble_nus_evt_t * p_evt);

#ifdef __cplusplus
}
#endif
#endif /*__COMMUNICATION_BLUETOOTH_H */
