

#ifndef __TPMS_H
#define __TPMS_H
#ifdef __cplusplus
 extern "C" {
#endif

		#include <stdint.h>
		#include "Communication_Bluetooth.h"
		
//		#define forTPMSinfoB9NRF_LOG											1
		#define IMPORT_WHITELIST_ARRAY_RAW_NUMBER					3
		
		typedef struct 
		{
			uint8_t DeviceAddress[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
		}WhiteListImport_t;
		
		/* TPMS Device Address Define*/
		#define TPMS_Device1	 	{0xD6, 0x01, 0x90, 0x54}
		#define TPMS_Device2		{0xC3, 0x01, 0x96, 0xF9}
		#define TPMS_Device3		{0xF5, 0x01, 0x91, 0x98}
		struct AEON_TPMS_LinkList
		{
			uint8_t DeviceID[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
			uint8_t DeviceNum;
			uint8_t PreviousTemperature;
			uint8_t PreviousPressure;
			struct AEON_TPMS_LinkList *p_NextNode;
		};
		
		void TPMSTask(void *pvParameters);
		
		uint32_t BeaconWhiteList_Set(WhiteListImport_t *p_ListImport);
		uint32_t BeaconWhiteList_Matching(MANUFACTURER_DATA_RECEIVE *p_manufacData);
		
		void BeaconWhiteList_Display(void);
		void BeaconWhiteList_Release(void);
		
#ifdef __cplusplus
}
#endif
#endif /*__WHITELIST_H */
